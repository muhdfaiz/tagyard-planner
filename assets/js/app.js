$(function() {
    var countTo = $(".count-to");

    if ( countTo.length ) {
        countTo.countTo({
            refreshInterval: 20
        });
    }

    var tooltips = $('[data-toggle="tooltip"]');

    if ( tooltips.length ) {
        tooltips.tooltip();
    }

});

$(function() {
    $('body').addClass('body_loaded');

    // Toggle sidebar dropdowns menus
    $('#sidebar__nav').on('show.bs.collapse', '.collapse', function() {
        var $this = $(this);

        $('#sidebar__nav').find('.collapse').not($this).each(function() {
            $(this).collapse('hide');
        });

    });

    // Sidebar: Toggle sidebar
    $("#sidebar__toggle, .sidebar__close").click(function() {
        $(".wrapper").toggleClass("alt");
        return false;
    });

    // Sidebar: Init perfect scrollbar
    $(".sidebar").perfectScrollbar({
        suppressScrollX: true
    });
});

// Initialize drag drop containers.
// Have 3 containers user can drag and drop
// - planner tags
// - own tags
// - others tags
// See https://github.com/bevacqua/dragula#readme for documentation.
var drake = dragula([document.getElementById('tag-container'), document.getElementById('own-tag-container'), document.getElementById('other-tag-container')]);

drake.on('drop', function (el, target, source, sibling) {
    var sourceID = $(source).attr('id');
    var targetID = $(target).attr('id');

    // Drag Drop For Own Tags

    // Check if tag drag from own tags and drop in tag container.
    if (sourceID === 'own-tag-container' && sourceID !== targetID && targetID === 'tag-container') {
        $(el).addClass('col-xs-12').removeClass('col-xs-6');

        var tagsID = [];

        $("#tag-container .tag-item").each(function( index ) {
            var tagID = $( this ).data('tag-id');
            tagsID.push(tagID);
        });

        var plannerID = $("input[name=planner_id]").val();
        updatePlannerTags(plannerID, tagsID);
    }

    // Check if user want to remove tag from tag container and drop in own tag container.
    if (sourceID === 'tag-container' && sourceID !== targetID && targetID === 'own-tag-container') {
        $(el).addClass('col-xs-12').removeClass('col-xs-6');

        var tagsID = [];

        $("#tag-container .tag-item").each(function( index ) {
            var tagID = $( this ).data('tag-id');
            tagsID.push(tagID);
        });

        var plannerID = $("input[name=planner_id]").val();
        updatePlannerTags(plannerID, tagsID);
    }

    if (sourceID === 'tag-container' && sourceID !== targetID) {
        $(el).addClass('col-xs-6').removeClass('col-xs-12');
    }

    if (sourceID === 'tag-container' && sourceID == targetID) {
        var tagsID = [];

        $("#tag-container .tag-item").each(function( index ) {
            var tagID = $( this ).data('tag-id');
            tagsID.push(tagID);
        });

        var plannerID = $("input[name=planner_id]").val();
        updatePlannerTags(plannerID, tagsID);
    }

    // Drag Drop For Other Tags
    if (sourceID === 'other-tag-container' && sourceID !== targetID && targetID === 'tag-container') {
        $(el).addClass('col-xs-12').removeClass('col-xs-6');

        var tagsID = [];

        $("#tag-container .tag-item").each(function( index ) {
            var tagID = $( this ).data('tag-id');
            tagsID.push(tagID);
        });

        var plannerID = $("input[name=planner_id]").val();
        updatePlannerTags(plannerID, tagsID);
    }

    if (sourceID === 'tag-container' && sourceID !== targetID && targetID === 'other-tag-container') {
        console.log('asdasd');
        $(el).addClass('col-xs-12').removeClass('col-xs-6');

        var tagsID = [];

        $("#tag-container .tag-item").each(function( index ) {
            var tagID = $( this ).data('tag-id');
            tagsID.push(tagID);
        });

        var plannerID = $("input[name=planner_id]").val();
        updatePlannerTags(plannerID, tagsID);
    }

    if (sourceID === 'tag-container' && sourceID !== targetID) {
        $(el).addClass('col-xs-6').removeClass('col-xs-12');
    }

    if (sourceID === 'tag-container' && sourceID == targetID) {
        var tagsID = [];

        $("#tag-container .tag-item").each(function( index ) {
            var tagID = $( this ).data('tag-id');
            tagsID.push(tagID);
        });

        var plannerID = $("input[name=planner_id]").val();
        updatePlannerTags(plannerID, tagsID);
    }
});

// Update planner tags when drag drop event happen.
// For example user want to drag his tags or other tags to the planner tag.
// System must assign that tag to the planner.
// Used in this page. http://localhost/tagyard_planner/planner/1/tag
function updatePlannerTags(plannerID, tagsID) {
    $.ajax({
        type: 'POST',
        url: base_url + 'planner/' + plannerID + '/tag/create',
        data: {
            token: $('meta[name="token"]').attr('content'),
            tags_id: tagsID
        },
        success: function (result) {
            console.log(result);
        }
    })
}

// Preview image used to display selected image before upload.
// Used in edit user profile to display preview image of avatar.
// See http://tagyard.dev/user/edit.
function previewImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.profile-image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

// Used in edit profile page to display preview image when user select avatar to upload.
// http://localhost/tagyard_planner/user/edit
$(".upload-button").change(function(){
    previewImage(this);
});

// Display toggle button on and off for planner status.
// You can set planner status public or private.
$("#planner_status").bootstrapSwitch({
    onColor: 'danger',
    offColor: 'info'
});

/**
 * Used to prevent form submit when user press enter because enter need to use
 * to insert new hash tags during create planner, update planner.
 * http://localhost/tagyard_planner/planner/create
 * During create new tags user need to press enter after search tag location here.
 * http://localhost/tagyard_planner/tag/create.
 */
(function disableFormSubmitWhenEnterOnInputTags() {
    $('#create-tag-form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $('#create-planner-form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $('#update-planner-form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
})();

// Auto complete tag title during search tags.
// See http://tagyard.dev/planner/1/tag
$('#search-tag-input').typeahead({
    minLength: 2,
    source: function (query, process) {
        console.log($("ul#tag-tabs li.active").attr('id'));
        return $.get(base_url + '/tag/autocomplete/own_tags', { keyword: query }, function (data) {
            return process(data);
        });
    },
    displayText: function (item) {
        return item.title;
    }
});

$('#search-tag-button').click(function(event){
    var keyword = $('#search-tag-input').val();

    window.location.href = window.location.href.split('?')[0] + "?keyword=" + keyword;
});

(function initializeTagMap() {
    if ($('#search-tag-location').length) {
        var mapOptions = {
            center: new google.maps.LatLng(3.1412, 101.68653),
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        tagMap = new google.maps.Map(document.getElementById("tag-map"), mapOptions);

        // Create the search box and link it to the UI element.
        var input = document.getElementById('search-tag-location');
        var searchBox = new google.maps.places.SearchBox(input);

        tagMap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        tagMap.addListener('bounds_changed', function() {
            searchBox.setBounds(tagMap.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();

            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: tagMap,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
                console.log(window.location.origin);
                var placeImageURL = window.location.origin + '/assets/img/placeholder.gif';

                if (place.photos) {
                    placeImageURL = place.photos[0].getUrl({ 'maxWidth': 500, 'maxHeight': 500 });
                }

                $('<input>').attr({
                    type: 'hidden',
                    id: 'location_image_url',
                    value: placeImageURL,
                    name: 'location_image_url'
                }).appendTo('#create-tag-form');

                $("#location_name").val(place.name);
                $("#latitude").val(place.geometry.location.lat());
                $("#longitude").val(place.geometry.location.lng());
            });

            tagMap.fitBounds(bounds);
        });
    }
})();

(function initTagImagesUploader() {
    $('#tag-files-input').fileuploader({
        maxSize: 20,
        extensions: ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'txt', 'xls', 'xlsx', 'mp4', 'mp3', 'mpg', '3gp', 'wmv'],
            changeInput: '<div class="fileuploader-input">' +
            '<div class="fileuploader-input-caption">' +
            '<span>Choose files to upload or drop files here</span>' +
            '</div><button type="button" class="btn btn-primary">CHOOSE FILES</button></div>',
        addMore: true
    });
})();

/*
 * Function to load uploaded files for tag.
 * For example user A already created new tag and contain two uploaded files.
 * When the user want to update the tag, system must display file uploaded previously.
 * See this URL: http://tagyard.dev/tag/1/edit
 */
(function initUploadedTagImages() {
    if ($('#update-tag-files-input').length) {

        var uploadedFiles = [];

        $(".uploaded-tag-file").each(function( index ) {
            uploadedFiles.push({
                'name': $(this).data('filename'),
                'size':  parseInt($(this).data('filesize')) * 1000,
                'file': '/uploads/tag_files/' + $(this).data('filename')
            });
        });

        $('#update-tag-files-input').fileuploader({
            maxSize: 20,
            extensions: ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'txt', 'xls', 'xlsx', 'mp4', 'mp3'],
            changeInput: '<div class="fileuploader-input">' +
                '<div class="fileuploader-input-caption">' +
                '<span>Choose files to upload or drop files here</span>' +
                '</div><button type="button" class="btn btn-primary">CHOOSE FILES</button></div>',
            addMore: true,
            files: uploadedFiles,
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                $.ajax({
                    type: 'POST',
                    url: base_url + 'tag_file/delete_by_filename',
                    data: {
                        token: $('meta[name="token"]').attr('content'),
                        filename: item.name
                    },
                    success: function (result) {
                        if (result) {
                            swal({
                                title: "Removed!",
                                text: "Tag file " + item.name + " has been removed from system.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            });
                        } else {
                            swal({
                                title: "Remove Failed!",
                                text: "Failed to remove tag file " + item.name + "from system.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            });
                        }
                    }
                })
            }
        });
    }
})();

/*
* Function to delete tag from database.
* User can delete tag during viewing list of tags.
* See this URL: http://tagyard.dev/tag
*/
$(".delete-tag").click(function(event) {
    event.preventDefault();

    var tagID = $(this).attr('data-id');

    swal({
            title: '',
            text: "Tag ID " + tagID + " will be removed from system. Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, remove it',
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: base_url + 'tag/delete',
                data: {
                    token: $('meta[name="token"]').attr('content'),
                    tag_id: tagID
                },

                success: function(result) {
                    if (result) {
                        swal("Deleted!", "");
                        swal({
                                title: "Removed!",
                                text: "Tag ID " + tagID + " has been removed from system.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    } else {
                        swal({
                                title: "Remove Failed!",
                                text: "Failed to remove tag ID " + tagID + "from system.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        });
});

/*
 * Function to delete planner from database.
 * User can delete their own planner during viewing list of planners.
 * See this URL: http://tagyard.dev/planner
 */
$(".delete-planner").click(function(event) {
    event.preventDefault();

    var plannerID = $(this).attr('data-id');

    swal({
            title: '',
            text: "Planner ID " + plannerID + " will be removed from system. Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, remove it',
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: base_url + 'planner/delete',
                data: {
                    token: $('meta[name="token"]').attr('content'),
                    planner_id: plannerID
                },

                success: function(result) {
                    if (result) {
                        swal("Deleted!", "");
                        swal({
                                title: "Removed!",
                                text: "Planner ID " + plannerID + " has been removed from system.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            },
                            function(){
                                location.reload();
                            });
                    } else {
                        swal({
                                title: "Remove Failed!",
                                text: "Failed to remove planner ID " + plannerID + "from system.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        });
});

/*
 * Initialize tag inputs in create planner form.
 * Used tag input to specify hashtags and co buddy in planner.
 * See this URL: http://tagyard.dev/planner/create
 */
(function initializeTagsInput() {
    $("#hashtags").tagsinput({
        tagClass: function() {
            return 'tag label label-info';
        }
    });

    $("#co_buddy").tagsinput({
        tagClass: function() {
            return 'tag label label-info';
        },
        itemValue: 'value',
        itemText: 'fullname',
        typeahead: {
            afterSelect: function(val) { this.$element.val(""); },
            source: function (query) {
                return jQuery.get(base_url + '/user/normal_users?name=' + query);
            }
        }
    });

    if ($('#current_co_buddy option').length) {
        $("#current_co_buddy option").each(function(index, element) {
            $("#co_buddy").tagsinput('add', { "value": $(this).val() , "fullname": $(this).text() });
        });
    }

})();

/*
 * Find center of google map between multiple points.
 * For example when you have multiple markers on the map and want find the center.
 */
function getCenterOfGoogleMapBetweenMultiplePoint(locations) {
    var bound = new google.maps.LatLngBounds();

    for (i = 0; i < locations.length; i++) {
        bound.extend( new google.maps.LatLng(locations[i][0], locations[i][1]) );
    }

    return bound.getCenter();
}

/*
 * Marker bounds used to set all markers will be display in the map.
 * Useful when you want to make all markers appear in google maps.
 * You cannot set map zoom with the fixed value because sometime
 * some marker will be hidden if the marker position beyond the zoom value.
 */
function setMarkerBounds(locations) {
    var markerBounds = new google.maps.LatLngBounds();

    for (i = 0; i < locations.length; i++) {
        markerBounds.extend( new google.maps.LatLng(locations[i][0], locations[i][1]) );
    }

    return markerBounds;
}

// Adds a marker to the map and push to the array.
function addMarker(location) {
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);
}

/*
 * Initialize map in tag review for active tag group tab in planner review page.
 * See this URL: http://tagyard.dev/planner/1/review
 */
(function initializeTagReviewMap() {
    // Get active tag group list.
    var activeTagList = $("ul#tag-list li.active");

    // Check if user in planner review page by checking
    // If true, initialize tag review map.
    if (activeTagList.length) {
        var tagGroupIndex = activeTagList.attr('data-tag-group-index');

        var tagGroupImages = $("ul#tag-list li.active .images");
        var originLatitude = tagGroupImages.attr('data-origin-latitude');
        var originLongitude = tagGroupImages.attr('data-origin-longitude');
        var destinationLatitude = tagGroupImages.attr('data-destination-latitude');
        var destinationLongitude = tagGroupImages.attr('data-destination-longitude');

        // Set origin coordinate and destination coordinate.
        var origin = new google.maps.LatLng(originLatitude, originLongitude),
            destination = new google.maps.LatLng(destinationLatitude, destinationLongitude);

        var mapElement = document.getElementById('tagReviewMap' + tagGroupIndex);

        var listOfLocations = [
            [originLatitude, originLongitude],
            [destinationLatitude, destinationLongitude]
        ];

        var centerOfMap = getCenterOfGoogleMapBetweenMultiplePoint(listOfLocations);

        // Map options. Set center of map.
        var mapOptions = {
            center: centerOfMap
        };

        var markerBounds = setMarkerBounds(listOfLocations);

        var plannerReviewMap = new google.maps.Map(mapElement, mapOptions);

        plannerReviewMap.fitBounds(markerBounds);

        $("#tagReviewMap" + tagGroupIndex).css("width", $('#tagReviewMap' + tagGroupIndex).parent().width()).css("height", 300);

        // Display origin marker in google map.
        var originMarker = new google.maps.Marker({
            position: origin,
            title: 'Hello World!',
            label: "A   ",
            animation: google.maps.Animation.DROP
        });

        originMarker.setMap(plannerReviewMap);

        // Display destination marker in google map.
        var destinationMarker = new google.maps.Marker({
            position: destination,
            title: 'Hello World!',
            label: "B",
            animation: google.maps.Animation.DROP
        });

        destinationMarker.setMap(plannerReviewMap);

        google.maps.event.trigger(plannerReviewMap, 'resize');
    }
})();

<<<<<<< Updated upstream
=======
(function initializeETAAndDistanceBetweenStartTagAndEndTag() {
    // Get active tag group list.
    var activeTagList = $("ul#tag-list li.active");

    // Check if user in planner review page by checking
    // If true, initialize tag review map.
    if (activeTagList.length) {
        var tagGroupIndex = activeTagList.attr('data-tag-group-index');

        var tagGroupImages = $("ul#tag-list li.active .images");
        var originLatitude = tagGroupImages.attr('data-origin-latitude');
        var originLongitude = tagGroupImages.attr('data-origin-longitude');
        var destinationLatitude = tagGroupImages.attr('data-destination-latitude');
        var destinationLongitude = tagGroupImages.attr('data-destination-longitude');

        // Set origin coordinate and destination coordinate.
        var origin = new google.maps.LatLng(originLatitude, originLongitude),
            destination = new google.maps.LatLng(destinationLatitude, destinationLongitude);

        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [origin, destination],
                destinations: [origin, destination],
                travelMode: 'DRIVING'
            }, callback);

        function callback(response, status) {
            if (status == 'OK') {
                console.log(response.rows[0].elements[1].duration.text);
                $("#eta" + tagGroupIndex).text(response.rows[0].elements[1].duration.text)
                $("#distance" + tagGroupIndex).text(response.rows[0].elements[1].duration.text)
            }
        }
    }
})();

>>>>>>> Stashed changes
$('a[class="tagGroupTab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target); // activated tab

    var tagGroupIndex = target.attr('data-tag-group-index');

    var tagGroupImages = $("ul#tag-list li.active .images");
    var originLatitude = tagGroupImages.attr('data-origin-latitude');
    var originLongitude = tagGroupImages.attr('data-origin-longitude');
    var destinationLatitude = tagGroupImages.attr('data-destination-latitude');
    var destinationLongitude = tagGroupImages.attr('data-destination-longitude');

    // Set origin coordinate and destination coordinate.
    var origin = new google.maps.LatLng(originLatitude, originLongitude),
        destination = new google.maps.LatLng(destinationLatitude, destinationLongitude);

    var mapElement = document.getElementById('tagReviewMap' + tagGroupIndex);

    var listOfLocations = [
        [originLatitude, originLongitude],
        [destinationLatitude, destinationLongitude]
    ];

    var centerOfMap = getCenterOfGoogleMapBetweenMultiplePoint(listOfLocations);

    // Map options. Set center of map.
    var mapOptions = {
        center: centerOfMap
    };

    var markerBounds = setMarkerBounds(listOfLocations);

    var tagReviewMap1 = new google.maps.Map(mapElement, mapOptions);

    $("#tagReviewMap" + tagGroupIndex).css("width", $('#tagReviewMap' + tagGroupIndex).parent().width()).css("height", 300);

    // Display origin marker in google map.
    var originMarker = new google.maps.Marker({
        position: origin,
        label: "A   ",
        animation: google.maps.Animation.DROP
    });

    originMarker.setMap(tagReviewMap1);

    // Display destination marker in google map.
    var destinationMarker = new google.maps.Marker({
        position: destination,
        label: "B",
        animation: google.maps.Animation.DROP
    });

    destinationMarker.setMap(tagReviewMap1);

    google.maps.event.trigger(tagReviewMap1, 'resize');

    tagReviewMap1.fitBounds(markerBounds);
<<<<<<< Updated upstream
});

$('a[class="tag-group-detail"]').on('shown.bs.tab', function (e) {
    var plannerSummaryIndex = $(this).attr('data-summary-index');

    if ($(this).attr('data-name')) {
        console.log(plannerSummaryIndex);
        // Set summary map to display all markers from all tags
        var summaryMapElement = document.getElementById('plannerSummaryMap' + plannerSummaryIndex);

        var allLocationsForSummaryMap = [];

        // Get all tags coordinate first
        $('.tag-group .images').each(function (index, value) {
            latitude = $(this).attr('data-origin-latitude');
            longitude = $(this).attr('data-origin-longitude');
            allLocationsForSummaryMap.push([latitude, longitude]);
        });

        var centerOfSummaryMap = getCenterOfGoogleMapBetweenMultiplePoint(allLocationsForSummaryMap);

        // Map options. Set center of map.
        var summaryMapOptions = {
            center: centerOfSummaryMap
        };

        var markerBoundsForReviewMap = setMarkerBounds(allLocationsForSummaryMap);

        var plannerSummaryMap = new google.maps.Map(summaryMapElement, summaryMapOptions);

        $("#plannerSummaryMap" + plannerSummaryIndex).css("width", $('#plannerSummaryMap' + plannerSummaryIndex).parent().width()).css("height", 300);

        for (var i = 0; i < allLocationsForSummaryMap.length; i++) {
            var markerOrders = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
            ];
            var position = new google.maps.LatLng(allLocationsForSummaryMap[i][0], allLocationsForSummaryMap[i][1]);

            var marker = new google.maps.Marker({
                position: position,
                label: markerOrders[i],
                animation: google.maps.Animation.DROP
            });

            marker.setMap(plannerSummaryMap);
        }

        google.maps.event.trigger(plannerSummaryMap, 'resize');

        plannerSummaryMap.fitBounds(markerBoundsForReviewMap);
    }
});
/**
 * Submit Tag Remark. For example when co buddy want to put remark on start tag or end tag.
 * Go to this URL http://tagyard.dev/planner/1/review and click any tag group and click on tab start tag
 * or end tag and try submit new remark.
 */
$( ".submit-tag-remark" ).click(function() {
    var plannerID = $(this).attr('data-planner-id');
    var tagID = $(this).attr('data-tag-id');
    var remarkContent = $("textarea[name=tag_remark" + tagID + "]").val();

    if (remarkContent.length === 0) {
        swal({
            title: "Failed to post new remark!",
            text: "Please fill up your remark in the textarea.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: "OK",
            closeOnConfirm: false
        });
    }

    $.ajax({
        type: 'POST',
        url: base_url + '/planner/' + plannerID + '/tag/' + tagID + '/remark',
=======

    // Set ETA
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            origins: [origin, destination],
            destinations: [origin, destination],
            travelMode: 'DRIVING'
        }, callback);

    function callback(response, status) {
        if (status == 'OK') {
            console.log(response.rows[0].elements[1].duration.text);
            $("#eta" + tagGroupIndex).text(response.rows[0].elements[1].duration.text)
        }
    }
});

$('a[class="tag-group-detail"]').on('shown.bs.tab', function (e) {
    var plannerSummaryIndex = $(this).attr('data-summary-index');

    if ($(this).attr('data-name')) {
        console.log(plannerSummaryIndex);
        // Set summary map to display all markers from all tags
        var summaryMapElement = document.getElementById('plannerSummaryMap' + plannerSummaryIndex);

        var allLocationsForSummaryMap = [];

        // Get all tags coordinate first
        $('.tag-group .images').each(function (index, value) {
            latitude = $(this).attr('data-origin-latitude');
            longitude = $(this).attr('data-origin-longitude');
            allLocationsForSummaryMap.push([latitude, longitude]);
        });

        var centerOfSummaryMap = getCenterOfGoogleMapBetweenMultiplePoint(allLocationsForSummaryMap);

        // Map options. Set center of map.
        var summaryMapOptions = {
            center: centerOfSummaryMap
        };

        var markerBoundsForReviewMap = setMarkerBounds(allLocationsForSummaryMap);

        var plannerSummaryMap = new google.maps.Map(summaryMapElement, summaryMapOptions);

        $("#plannerSummaryMap" + plannerSummaryIndex).css("width", $('#plannerSummaryMap' + plannerSummaryIndex).parent().width()).css("height", 300);

        for (var i = 0; i < allLocationsForSummaryMap.length; i++) {
            var markerOrders = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
            ];
            var position = new google.maps.LatLng(allLocationsForSummaryMap[i][0], allLocationsForSummaryMap[i][1]);

            var marker = new google.maps.Marker({
                position: position,
                label: markerOrders[i],
                animation: google.maps.Animation.DROP
            });

            marker.setMap(plannerSummaryMap);
        }

        google.maps.event.trigger(plannerSummaryMap, 'resize');

        plannerSummaryMap.fitBounds(markerBoundsForReviewMap);
    }
});
/**
 * Submit Tag Remark. For example when co buddy want to put remark on start tag or end tag.
 * Go to this URL http://tagyard.dev/planner/1/review and click any tag group and click on tab start tag
 * or end tag and try submit new remark.
 */
$( ".submit-tag-remark" ).click(function() {
    var plannerID = $(this).attr('data-planner-id');
    var tagID = $(this).attr('data-tag-id');
    var remarkContent = $("textarea[name=tag_remark" + tagID + "]").val();

    if (remarkContent.length === 0) {
        swal({
            title: "Failed to post new remark!",
            text: "Please fill up your remark in the textarea.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: "OK",
            closeOnConfirm: false
        });
    }

    $.ajax({
        type: 'POST',
        url: '/planner/' + plannerID + '/tag/' + tagID + '/remark',
        data: {
            token: $('meta[name="token"]').attr('content'),
            remark: remarkContent
        },
        success: function(result) {
            if (result.hasOwnProperty('success')) {
                console.log(result.success);
                $(".tag-remark-container" + tagID).append('<li class="media"><a href="#" class="pull-left">' +
                    '<img src="' + result.success.user_avatar + '" alt="" class="img-circle"></a><div class="media-body">' +
                    '<span class="text-muted pull-right"><small class="text-muted">' + result.success.created_at + '</small></span>' +
                    '<strong class="text-success">' + result.success.fullname + '</strong><p>' + result.success.content + '</p></div></li>');
            } else {
                swal({
                    title: "Failed to post new remark!",
                    text: result.error,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
            }
        }
    });
});

/**
 * Submit Planner Remark. For example when co buddy want to put remark on certain planner.
 * Go to this URL http://tagyard.dev/planner/1/review and click any tag group and click on summary tab
 * and submit new remark.
 * Documentation URL: http://plugins.krajee.com/star-rating
 *
 */
$( ".submit-planner-remark" ).click(function() {
    var key = $(this).attr('data-planner-key');
    var plannerID = $(this).attr('data-planner-id');
    var remarkContent = $("textarea[name=planner_remark" + plannerID + "_" + key +"]").val();

    if (remarkContent.length === 0) {
        swal({
            title: "Failed to post new remark!",
            text: "Please fill up your remark in the textarea.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: "OK",
            closeOnConfirm: false
        });
    }

    $.ajax({
        type: 'POST',
        url: '/planner/' + plannerID + '/remark',
>>>>>>> Stashed changes
        data: {
            token: $('meta[name="token"]').attr('content'),
            remark: remarkContent
        },
        success: function(result) {
<<<<<<< Updated upstream
            if (result.hasOwnProperty('success')) {
                console.log(result.success);
                $(".tag-remark-container" + tagID).append('<li class="media"><a href="#" class="pull-left">' +
=======
            console.log(".planner-remark-container" + plannerID + '-' + key);
            if (result.hasOwnProperty('success')) {
                console.log(result.success);
                $(".planner-remark-container" + plannerID + '-' + key).append('<li class="media"><a href="#" class="pull-left">' +
>>>>>>> Stashed changes
                    '<img src="' + result.success.user_avatar + '" alt="" class="img-circle"></a><div class="media-body">' +
                    '<span class="text-muted pull-right"><small class="text-muted">' + result.success.created_at + '</small></span>' +
                    '<strong class="text-success">' + result.success.fullname + '</strong><p>' + result.success.content + '</p></div></li>');
            } else {
                swal({
                    title: "Failed to post new remark!",
                    text: result.error,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
            }
        }
    });
});

/**
<<<<<<< Updated upstream
 * Submit Planner Remark. For example when co buddy want to put remark on certain planner.
 * Go to this URL http://tagyard.dev/planner/1/review and click any tag group and click on summary tab
 * and submit new remark.
 * Documentation URL: http://plugins.krajee.com/star-rating
 *
 */
$( ".submit-planner-remark" ).click(function() {
    var key = $(this).attr('data-planner-key');
    var plannerID = $(this).attr('data-planner-id');
    var remarkContent = $("textarea[name=planner_remark" + plannerID + "_" + key +"]").val();

    if (remarkContent.length === 0) {
        swal({
            title: "Failed to post new remark!",
            text: "Please fill up your remark in the textarea.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: "OK",
            closeOnConfirm: false
        });
    }

    $.ajax({
        type: 'POST',
        url: base_url + '/planner/' + plannerID + '/remark',
        data: {
            token: $('meta[name="token"]').attr('content'),
            remark: remarkContent
        },
        success: function(result) {
            console.log(".planner-remark-container" + plannerID + '-' + key);
            if (result.hasOwnProperty('success')) {
                console.log(result.success);
                $(".planner-remark-container" + plannerID + '-' + key).append('<li class="media"><a href="#" class="pull-left">' +
                    '<img src="' + result.success.user_avatar + '" alt="" class="img-circle"></a><div class="media-body">' +
                    '<span class="text-muted pull-right"><small class="text-muted">' + result.success.created_at + '</small></span>' +
                    '<strong class="text-success">' + result.success.fullname + '</strong><p>' + result.success.content + '</p></div></li>');
            } else {
                swal({
                    title: "Failed to post new remark!",
                    text: result.error,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
            }
        }
    });
});

/**
=======
>>>>>>> Stashed changes
 * Tag rating. For example co buddy want to give rating for start tag during planner review.
 * Go to this URL http://tagyard.dev/planner/1/review and click any tag group and click on start tag tab
 * and give rating.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.start-tag-rating').on('rating.change', function(event, value, caption) {
    var tagID = $(this).attr('data-tag-id');
        plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'POST',
<<<<<<< Updated upstream
        url: base_url + '/planner/' + plannerID + '/tag/' + tagID + '/rating',
=======
        url: '/planner/' + plannerID + '/tag/' + tagID + '/rating',
>>>>>>> Stashed changes
        data: {
            token: $('meta[name="token"]').attr('content'),
            value: value
        },
        success: function(result) {
            console.log(result);
        }
    });
});

/**
 * Tag rating. For example co buddy want to giv rating for end tag during planner review.
 * Go to this URL http://tagyard.dev/planner/1/review and click any tag group and click on end tag tab
 * and give rating.
 */
$('.end-tag-rating').on('rating.change', function(event, value, caption) {
    var tagID = $(this).attr('data-tag-id');
        plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'POST',
<<<<<<< Updated upstream
        url: base_url + '/planner/' + plannerID + '/tag/' + tagID + '/rating',
=======
        url: '/planner/' + plannerID + '/tag/' + tagID + '/rating',
>>>>>>> Stashed changes
        data: {
            token: $('meta[name="token"]').attr('content'),
            value: value
        },
        success: function(result) {
            console.log(result);
        }
    });
});
<<<<<<< Updated upstream

/**
 * Delete rating for start tag.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.start-tag-rating').on('rating.clear', function(event) {
    var tagID = $(this).attr('data-tag-id');
    plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'GET',
        url: base_url + '/planner/' + plannerID + '/tag/' + tagID + '/rating/delete',
        data: {
            token: $('meta[name="token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
        }
    });
});

/**
 * Delete rating for end tag.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.end-tag-rating').on('rating.clear', function(event) {
    var tagID = $(this).attr('data-tag-id');
    plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'GET',
        url: base_url + '/planner/' + plannerID + '/tag/' + tagID + '/rating/delete',
        data: {
            token: $('meta[name="token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
        }
    });
});

/**
 * Planner rating. For example co buddy want to give rating for certain planner during planner review.
 * Go to this URL http://tagyard.dev/planner/1/review and click any tag group and click on summary tab
 * and give rating.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.planner-rating').on('rating.change', function(event, value, caption) {
    var plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'POST',
        url: base_url + '/planner/' + plannerID + '/rating',
        data: {
            token: $('meta[name="token"]').attr('content'),
            value: value
        },
        success: function(result) {
            console.log(result);
        }
    });
});

/**
 * Delete rating for planner.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.planner-rating').on('rating.clear', function(event) {
    var plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'GET',
        url: base_url + '/planner/' + plannerID + '/rating/delete',
=======

/**
 * Delete rating for start tag.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.start-tag-rating').on('rating.clear', function(event) {
    var tagID = $(this).attr('data-tag-id');
    plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'GET',
        url: '/planner/' + plannerID + '/tag/' + tagID + '/rating/delete',
        data: {
            token: $('meta[name="token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
        }
    });
});

/**
 * Delete rating for end tag.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.end-tag-rating').on('rating.clear', function(event) {
    var tagID = $(this).attr('data-tag-id');
    plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'GET',
        url: '/planner/' + plannerID + '/tag/' + tagID + '/rating/delete',
        data: {
            token: $('meta[name="token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
        }
    });
});

/**
 * Planner rating. For example co buddy want to give rating for certain planner during planner review.
 * Go to this URL http://tagyard.dev/planner/1/review and click any tag group and click on summary tab
 * and give rating.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.planner-rating').on('rating.change', function(event, value, caption) {
    var plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'POST',
        url: '/planner/' + plannerID + '/rating',
        data: {
            token: $('meta[name="token"]').attr('content'),
            value: value
        },
        success: function(result) {
            console.log(result);
        }
    });
});

/**
 * Delete rating for planner.
 * Documentation URL: http://plugins.krajee.com/star-rating
 */
$('.planner-rating').on('rating.clear', function(event) {
    var plannerID = $(this).attr('data-planner-id');

    $.ajax({
        type: 'GET',
        url: '/planner/' + plannerID + '/rating/delete',
>>>>>>> Stashed changes
        data: {
            token: $('meta[name="token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
        }
    });
});

