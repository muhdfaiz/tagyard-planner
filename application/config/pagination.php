<?php

//config for bootstrap pagination class integration
$config['pagination']['full_tag_open'] = "<ul class='pagination'>";
$config['pagination']['full_tag_close'] ="</ul>";
$config['pagination']['num_tag_open'] = '<li>';
$config['pagination']['num_tag_close'] = '</li>';
$config['pagination']['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
$config['pagination']['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
$config['pagination']['next_tag_open'] = "<li>";
$config['pagination']['next_tagl_close'] = "</li>";
$config['pagination']['prev_tag_open'] = "<li>";
$config['pagination']['prev_tagl_close'] = "</li>";
$config['pagination']['first_tag_open'] = "<li>";
$config['pagination']['first_tagl_close'] = "</li>";
$config['pagination']['last_tag_open'] = "<li>";
$config['pagination']['last_tagl_close'] = "</li>";