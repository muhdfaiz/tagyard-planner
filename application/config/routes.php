<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Auth URL
$route['auth/login'] = 'auth/login';
$route['auth/forgot_password'] = 'auth/forgotPassword';
$route['auth/reset_password/(:any)'] = 'auth/resetPassword/$1';
$route['auth/register'] = 'register/index';

// Dashboard routes
$route['dashboard'] = 'dashboard/index';

// User routes
$route['user/edit'] = 'user/edit';
$route['user/normal_users'] = 'user/searchNormalUsersForAutoComplete';

$route['planner'] = 'planner/index';
$route['planner/create'] = 'planner/create';
$route['planner/(:num)/update'] = 'planner/update/$1';
$route['planner/delete'] = "planner/delete";

// Tag Routes
$route['tag/create'] = 'tag/create';
$route['tag'] = 'tag/index';
$route['tag/(:num)/update'] = "tag/update/$1";
$route['tag/autocomplete/own_tags'] = "tag/autoCompleteOwnTags";
$route['tag/delete'] = "tag/delete";

// Tag Planner
$route['planner/(:num)/tag'] = 'tagPlanner/index/$1';
$route['planner/(:num)/tag/create'] = 'tagPlanner/create/$1';

// Tag File routes
$route['tag_file/delete_by_filename'] = "tagFile/deleteByFilename";

// Planner Review Routes
$route['planner/(:num)/review'] = "plannerReview/review/$1";

// Tag Remark route
$route['planner/(:num)/tag/(:num)/remark'] = "remark/createTagRemark/$1/$2";

// Planner Remark route
$route['planner/(:num)/remark'] = 'remark/createPlannerRemark/$1';

// Tag Rating route
$route['planner/(:num)/tag/(:num)/rating'] = "rating/createTagRating/$1/$2";
$route['planner/(:num)/tag/(:num)/rating/delete'] = "rating/deleteTagRating/$1/$2";

// Planner rating
$route['planner/(:num)/rating'] = 'rating/createPlannerRating/$1';
$route['planner/(:num)/rating/delete'] = 'rating/deletePlannerRating/$1';