<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    /**
     * Logged In User Avatar
     *
     * @var $avatar
     */
    public $avatar;

    /**
     * Logged In User Fullname
     *
     * @var $fullname
     */
    public $fullname;

    /**
     * MY_Controller constructor.
     * Load ion_auth_library and set fullname and avatar to make
     * the data available in all controller and able to pass to view.
     * Avatar and fullname will be used in all views because need to display
     * in sidebar.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->setAvatarAndFullname();
    }

    /**
     * Check user not logg
     */
    protected function redirectUserToLoginPageIfNotLogin()
    {
        // Check If User Logged In Or Not. If not logged in redirect to login page
        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('auth/login'), 'refresh');
            return;
        }
    }

    /**
     * Check user already logged in or not. If user already logged in return to dashboard.
     */
    protected function redirectUserToDashboardIfLoggedIn()
    {
        // Check If User Logged In Or Not. If not logged in redirect to login page
        if ($this->ion_auth->logged_in()) {
            redirect(base_url('dashboard'), 'refresh');
            return;
        }
    }

    protected function setAvatarAndFullname()
    {
        if ($this->ion_auth->logged_in()) {
            $user = $this->ion_auth->user()->row();

            $this->fullname = $user->first_name . ' ' . $user->last_name;
            $this->avatar = $user->avatar;
        }
    }

}