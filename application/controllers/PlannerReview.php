<?php

class PlannerReview extends MY_Controller
{

    /**
     * PlannerReview constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
        $this->load->model('planner_tag_model');
        $this->load->model('tag_remark_model');
        $this->load->model('planner_remark_model');
        $this->load->model('tag_rating_model');
        $this->load->model('planner_rating_model');
        $this->load->model('tag_file_model');
        $this->load->model('user_planner_model');
        $this->load->model('planner_tag_model');
        $this->load->model('planner_model');
    }

    public function review($plannerID)
    {
        $data['title'] = 'Planner Review';

        $plannerTags = $this->planner_tag_model->getByPlannerID($plannerID);

        $tagGroups = [];

        $tagRemarks = [];

        foreach ($plannerTags as $key => $plannerTag) {
            // Group planner tags by first tag and second tag.
            // Next second tag and third tag and next until end of tag.
            if (isset($plannerTags[$key + 1])) {
                // Get tag files for each tag.
                $plannerTags[$key]['files'] = $this->tag_file_model->getByTagID($plannerTags[$key]['id']);
                $plannerTags[$key + 1]['files'] = $this->tag_file_model->getByTagID($plannerTags[$key + 1]['id']);

                $tags = [
                    $plannerTags[$key],
                    $plannerTags[$key + 1]
                ];

                $tagGroups[] = $tags;
            }

            // Get tag remarks for all tags for this planner
             $tagRemarksForTag= $this->tag_remark_model->getByPlannerIDAndTagID($plannerID, $plannerTag['tag_id']);

            // Convert created_at datetime for tag remarks to human readable times. For example 13 minutes ago.
            foreach ($tagRemarksForTag as $key1 => $tagRemarkForTag) {
                $createdAt = new DateTime($tagRemarkForTag['created_at']);
                $createdAtInSecond =  $createdAt->format('U');
                $now = time();

                $tagRemarksForTag[$key1]['created_at'] = strtolower(timespan($createdAtInSecond, $now) . ' ago');
            }

            $tagRemarks[$plannerTag['tag_id']] = $tagRemarksForTag;
        }

        // Get planner remarks for this planner
        $plannerRemarks = $this->planner_remark_model->getByPlannerID($plannerID);

        // Convert created_at datetime for planner remarks to human readable times. For example 13 minutes ago.
        foreach ($plannerRemarks as $key2 => $plannerRemark) {
            $createdAt = new DateTime($plannerRemark['created_at']);
            $createdAtInSecond =  $createdAt->format('U');
            $now = time();

            $plannerRemarks[$key2]['created_at'] = strtolower(timespan($createdAtInSecond, $now) . ' ago');
        }

        $loggedInUser = $this->ion_auth->user()->row();

        // Get Tag Ratings give by logged in user.
        $userTagRatings = $this->tag_rating_model->getByUserIDAndPlannerID($loggedInUser->id, $plannerID);

        $userTagRatingsWithTagIDAsKey = [];

        // Replace array key for user tag ratings with tag id.
        foreach ($userTagRatings as $key3 => $userTagRating) {
            $userTagRatingsWithTagIDAsKey[$userTagRating['tag_id']] = $userTagRating;
        }

        // Get planner ratings give by logged in user.
        $userPlannerRating = $this->planner_rating_model->getByUserIDAndPlannerID($loggedInUser->id, $plannerID);

<<<<<<< Updated upstream
=======
        $totalDistanceInKM = 0;
        $totalETAInSecond = 0;

        foreach ($tagGroups as $key => $tagGroup) {
            $distanceMatrix =json_decode(file_get_contents(
                'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $tagGroup[0]['latitude']
                . ',' . $tagGroup[0]['longitude'] . '&destinations=' . $tagGroup[1]['latitude'] . ',' . $tagGroup[1]['longitude']
                . '&key=AIzaSyC0fw7XFKcPLiMOWSYz9yP56ai6dSKPZ6E'
            ), true);
            $distance = (int)($distanceMatrix['rows'][0]['elements'][0]['distance']['value'] / 1000) ;

            $totalETAInSecond += $distanceMatrix['rows'][0]['elements'][0]['duration']['value'];
            $totalDistanceInKM += $distance;
        }

        // Convert second to human readable duration. For example 2 days 5 hours
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$totalETAInSecond");
        $totalETA = $dtF->diff($dtT)->format('%a days, %h hours %i minutes');

>>>>>>> Stashed changes
        $data['plannerID'] = $plannerID;
        $data['planner'] = json_decode(json_encode($this->user_planner_model->getByPlannerID($plannerID)), true);
        $data['planner']['co_buddy'] = $this->planner_model->getPlannerCoBuddies($plannerID, $loggedInUser->id);
        $data['tags'] = $plannerTags;
        $data['tagGroups'] = $tagGroups;
        $data['tagRemarks'] = $tagRemarks;
        $data['plannerRemarks'] = $plannerRemarks;
        $data['total_tags'] = count($plannerTags);
        $data['userTagRatings'] = $userTagRatingsWithTagIDAsKey;
        $data['userPlannerRating'] = $userPlannerRating;
<<<<<<< Updated upstream
=======
        $data['totalETA'] = $totalETA;
        $data['totalDistance'] = $totalDistanceInKM;
>>>>>>> Stashed changes

        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/sidebar');
        $this->load->view('pages/planner_review/index', $data);
        $this->load->view('layouts/footer');
    }

}