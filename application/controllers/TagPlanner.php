<?php

class TagPlanner extends MY_Controller
{
    /**
     * TagPlanner constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
        $this->load->library('form_validation');
        $this->load->model('tag_model');
        $this->load->model('tag_file_model');
        $this->load->model('planner_model');
        $this->load->model('user_planner_model');
        $this->load->model('planner_tag_model');
    }

    public function index($plannerID)
    {
        $loggedInUser = $this->ion_auth->user()->row();

        $keyword = $this->input->get('keyword');

        $data['title'] = 'Manage Planner Tags';
        $data['own_tags'] = $this->tag_model->getTagsNotAssignToPlannerByUserID($loggedInUser->id, $plannerID, $keyword);
        $data['other_tags'] = $this->tag_model->getOtherTagsNotAssignToPlanner($loggedInUser->id, $plannerID, $keyword);
        $data['planner'] = json_decode(json_encode($this->user_planner_model->getByPlannerID($plannerID)), true);
        $data['planner']['co_buddy'] = $this->planner_model->getPlannerCoBuddies($plannerID, $loggedInUser->id);
        $data['tags'] = $this->planner_tag_model->getByPlannerID($plannerID);
        $data['keyword'] = $keyword;

        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/sidebar');
        $this->load->view('pages/tag_planner/index');
        $this->load->view('layouts/footer');
    }

    public function create($plannerID)
    {
        $tagsID = $this->input->post('tags_id');

        $inputData = [];

        foreach ($tagsID as $key => $tagID) {
            $inputData[$key] = [
                'planner_id' => $plannerID,
                'tag_id' => $tagID,
                'order' => $key + 1
            ];
        }

        $this->planner_tag_model->deleteByPlannerID($plannerID);

        $this->planner_tag_model->createMultiple($inputData);

        if ($this->db->affected_rows() != '1') {
            header('Content-Type: application/json');

            echo false;
            return;
        }

        header('Content-Type: application/json');

        echo true;
        return;
    }
}