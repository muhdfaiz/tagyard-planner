<?php

class Remark extends MY_Controller
{

    /**
     * Remark constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
        $this->load->library('form_validation');
        $this->load->model('tag_model');
        $this->load->model('planner_model');
        $this->load->model('tag_remark_model');
        $this->load->model('planner_remark_model');
    }

    /**
     * Create new tag remark.
     * For example when co buddy want post new remark in start
     * tag or end tag during planner review.
     *
     * @param $plannerID
     * @param $tagID
     */
    public function createTagRemark($plannerID, $tagID)
    {
        $remarkContent = $this->input->post('remark');

        if (!empty($remarkContent)) {
            // Check if planner ID exist. If not exist return an error.
            $planner = $this->planner_model->getByID($plannerID);

            if (empty($planner)) {
                header('Content-Type: application/json');

                $errorMessage = ['error' => 'Planner does not exist.'];
                echo json_encode($errorMessage);
                return;
            }

            // Check if tag ID exist. If not exist return an error.
            $tag = $this->tag_model->getByID($tagID);

            if (empty($tag)) {
                header('Content-Type: application/json');

                $errorMessage = ['error' => 'Tag does not exist.'];
                echo json_encode($errorMessage);
                return;
            }

            $loggedInUser = $this->ion_auth->user()->row();


            // Set tag remark data
            $remark = [
                'user_id' => $loggedInUser->id,
                'planner_id' => $plannerID,
                'tag_id' => $tagID,
                'content' => $remarkContent,
            ];

            $this->tag_remark_model->create($remark);

            if ($this->db->affected_rows() == '1') {
                // Get tag remark detail
                $insertID = $this->db->insert_id();
                $tagRemark = $this->tag_remark_model->getByID($insertID);

                $tagRemarkCreatedAt = new DateTime($tagRemark->created_at);
                $tagRemarkCreatedAtInSecond =  $tagRemarkCreatedAt->format('U');
                $now = time();

                header('Content-Type: application/json');

                $errorMessage = [
                    'success' => [
                        'user_avatar' => base_url('uploads/' . $loggedInUser->avatar),
                        'fullname' => $loggedInUser->first_name . ' ' . $loggedInUser->last_name,
                        'content' => $remarkContent,
                        'created_at' => strtolower(timespan($tagRemarkCreatedAtInSecond, $now) . ' ago')
                    ]
                ];
                echo json_encode($errorMessage);
                return;
            }

            header('Content-Type: application/json');

            $errorMessage = ['error' => 'Failed to created new remark.'];
            echo json_encode($errorMessage);
            return;
        }

        header('Content-Type: application/json');

        $errorMessage = ['error' => 'Please make sure remark not empty.'];
        echo json_encode($errorMessage);
    }

    /**
     * Create new planner remark.
     * For example when co buddy want post new remark in certain planner.
     *
     * @param $plannerID
     */
    public function createPlannerRemark($plannerID)
    {
        $remarkContent = $this->input->post('remark');

        if (!empty($remarkContent)) {
            // Check if planner ID exist. If not exist return an error.
            $planner = $this->planner_model->getByID($plannerID);

            if (empty($planner)) {
                header('Content-Type: application/json');

                $errorMessage = ['error' => 'Planner does not exist.'];
                echo json_encode($errorMessage);
                return;
            }

            $loggedInUser = $this->ion_auth->user()->row();

            // Set tag remark data
            $remark = [
                'user_id' => $loggedInUser->id,
                'planner_id' => $plannerID,
                'content' => $remarkContent,
            ];

            $this->planner_remark_model->create($remark);

            if ($this->db->affected_rows() == '1') {
                // Get planner remark detail
                $insertID = $this->db->insert_id();
                $plannerRemark = $this->planner_remark_model->getByID($insertID);

                // Set planner remark created to human readable time. For example 3 minutes ago.
                $plannerRemarkCreatedAt = new DateTime($plannerRemark->created_at);
                $plannerRemarkCreatedAtInSecond =  $plannerRemarkCreatedAt->format('U');
                $now = time();

                header('Content-Type: application/json');

                $errorMessage = [
                    'success' => [
                        'user_avatar' => base_url('uploads/' . $loggedInUser->avatar),
                        'fullname' => $loggedInUser->first_name . ' ' . $loggedInUser->last_name,
                        'content' => $remarkContent,
                        'created_at' => strtolower(timespan($plannerRemarkCreatedAtInSecond, $now) . ' ago')
                    ]
                ];
                echo json_encode($errorMessage);
                return;
            }

            header('Content-Type: application/json');

            $errorMessage = ['error' => 'Failed to created new remark.'];
            echo json_encode($errorMessage);
            return;
        }

        header('Content-Type: application/json');

        $errorMessage = ['error' => 'Please make sure remark not empty.'];
        echo json_encode($errorMessage);
    }
}