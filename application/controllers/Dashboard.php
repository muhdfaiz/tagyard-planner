<?php

class dashboard extends MY_Controller
{

    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
    }

    /**
     * Display dashboard page.
     */
    public function index()
    {
        $data['title'] = 'Dashboard';

        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/sidebar');
        $this->load->view('pages/dashboard/index');
        $this->load->view('layouts/footer');
    }

}