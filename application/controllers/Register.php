<?php

class Register extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
    }

//    public function index()
//    {
//        $this->load->helper(array('form', 'url'));
//
//        $this->load->library('form_validation');
//
//        // Set validation rule for email and password.
//        $this->form_validation->set_rules('email', 'email', 'required');
//        $this->form_validation->set_rules('password', 'password', 'required');
//        $this->form_validation->set_rules('confirm_password', 'password', 'required');
//
//        if ($this->form_validation->run() == FALSE) {
//            $data['title'] = 'Register';
//
//            $this->load->view('register/register', $data);
//        }
//
//        $this->processRegister();
//    }

    public function index()
    {
        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', 'firstname', 'required');
        $this->form_validation->set_rules('last_name', 'lastname', 'required');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');

        $this->form_validation->set_rules('password', 'password', 'required|min_length[' .
            $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' .
            $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', 'confirm password:', 'required');

        if ($this->form_validation->run() == true)
        {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
            );

            if ($this->ion_auth->register($identity, $password, $email, $additional_data)) {
                $this->session->set_flashdata('message', $this->ion_auth->messages());

                redirect("auth/login", 'refresh');
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->errors());

                redirect("auth/login", 'refresh');
            }


        } else {
            $data['title'] = 'Register';

            $this->load->view('register/register', $data);
        }
    }


}