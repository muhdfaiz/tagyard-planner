<?php

class TagFile extends MY_Controller
{

    /**
     * Planner constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
        $this->load->model('tag_file_model');
    }

    /**
     * Delete tag file using filename
     */
    public function deleteByFilename()
    {
        $tagFilename = $this->input->post('filename');

        $tagFile = $this->tag_file_model->getByFilename($tagFilename);

        if ($tagFile->id) {
            $result = $this->tag_file_model->deleteByFilename($tagFile->filename);

            header('Content-Type: application/json');

            if ($result) {
                unlink(FCPATH . '/uploads/tag_files/' . $tagFile->filename);
                echo true;
                return;
            }

            echo false;
            return;
        }

        header('Content-Type: application/json');
        echo false;
        return;
    }

}