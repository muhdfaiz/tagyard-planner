<?php

class Planner extends MY_Controller
{

    /**
     * Planner constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
        $this->load->library('form_validation');
        $this->load->model('planner_model');
        $this->load->model('user_model');
        $this->load->model('user_planner_model');
        $this->load->library('pagination');
    }

    /**
     * List all of planner created by logged in user.
     */
    public function index($offset = 0)
    {
        (isset($_GET['limit']) ? $limit = $_GET['limit'] : $limit = 10);

        $loggedInUser = $this->ion_auth->user()->row();

        $data['user'] = $loggedInUser;
        $data['total_planners'] = $this->user_planner_model->countByUserID($loggedInUser->id);
        $data['planners'] = $this->user_planner_model->getByUserID($loggedInUser->id, $limit, $offset);
        $data['pagination'] = $this->setPagination(base_url() . "planner/", $data['total_planners'],
            $limit, $urlSegment = 2);
        $data['limit'] = $limit;

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $data['total_tags'];
        }

        foreach ($data['planners'] as $key => $planner) {
            $data['planners'][$key]['co_buddy'] = $this->planner_model->getPlannerCoBuddies($planner['id'], $loggedInUser->id);
        }

        $data['title'] = 'Planner List';

        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/sidebar');
        $this->load->view('pages/planner/index');
        $this->load->view('layouts/footer');
    }

    /**
     * Create new planner for logged in user and store in database.
     */
    public function create()
    {
        // Set validation rules.
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('hashtags[]', 'hashtags', 'required');

        if ($this->form_validation->run() === TRUE) {
            // Retrieve logged in user data.
            $loggedInUser = $this->ion_auth->user()->row();

            $coBuddiesID = $this->input->post('co_buddy');

            // If co buddy input not empty check if co buddy exist in the system.
            // If not exist return an error.
            if (!empty($coBuddiesID)) {
                foreach ($coBuddiesID as $key => $coBuddyID) {
                    $coBuddy = $this->user_model->getByID($coBuddyID);

                    if (!isset($coBuddy->id)) {
                        $this->session->set_flashdata('error', 'One of the co buddy does not exist in system.' );

                        redirect('/planner/create', 'refresh');
                        return;
                    }
                }
            }

            // Set planner data.
            $plannerData = [
                'user_id' => $loggedInUser->id,
                'name' => $this->input->post('name'),
                'hashtags' => json_encode($this->input->post('hashtags')),
                'status'  => !empty($this->input->post('status')) ? $this->input->post('status') : 0
            ];

            // Store new planner in database.
            $this->planner_model->create($plannerData);

            // Retrieve newly created planner id.
            $plannerID = $this->db->insert_id();

            // If failed to create planner redirect to create planner page and display
            // error message.
            if ($this->db->affected_rows() != '1') {
                $this->session->set_flashdata('error', 'Failed to create new planner.' );

                redirect('/planner/create', 'refresh');
                return;
            }

            // Add current logged in user in co buddies  data.
            $coBuddiesData[] = [
                'planner_id' => $plannerID,
                'user_id' => $loggedInUser->id,
            ];

            // Add other co buddy from co_buddy inputs into co buddies data.
            foreach ($coBuddiesID as $key => $coBuddyID) {
                $coBuddiesData[] = [
                    'planner_id' => $plannerID,
                    'user_id' => $coBuddyID,
                ];
            }

            $this->user_planner_model->createMultiple($coBuddiesData);

            // If failed to create planner buddy redirect to create planner page and display
            // error message.
            if ($this->db->affected_rows() < 1) {
                $this->session->set_flashdata('error', 'Failed to create planner buddy.' );

                redirect('/planner/create', 'refresh');
                return;
            }

            $this->session->set_flashdata('message', 'Successfully created new tag.' );

            redirect('/planner', 'refresh');

        } else {
            // Load page to create planner.
            $data['title'] = 'Create New Planner';

            $this->load->view('layouts/header', $data);
            $this->load->view('layouts/sidebar');
            $this->load->view('pages/planner/create');
            $this->load->view('layouts/footer');
        }
    }

    /**
     * Update planner.
     */
    public function update($plannerID)
    {
        // Set validation rules.
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('hashtags[]', 'hashtags', 'required');

        // Retrieve logged in user data.
        $loggedInUser = $this->ion_auth->user()->row();

        $data['planner'] = $this->planner_model->getByID($plannerID);
        $data['planner_buddies'] = $this->planner_model->getPlannerCoBuddies($plannerID, $loggedInUser->id);

        if ($this->form_validation->run() === TRUE) {
            $coBuddiesID = $this->input->post('co_buddy');

            // If co buddy input not empty check if co buddy exist in the system.
            // If not exist return an error.
            if (!empty($coBuddiesID)) {
                foreach ($coBuddiesID as $key => $coBuddyID) {
                    $coBuddy = $this->user_model->getByID($coBuddyID);

                    if (!isset($coBuddy->id)) {
                        $this->session->set_flashdata('error', 'One of the co buddy does not exist in system.' );

                        redirect('/planner/' . $plannerID . '/update', 'refresh');
                        return;
                    }
                }
            }

            // Set planner data.
            $plannerData = [
                'name' => $this->input->post('name'),
                'hashtags' => json_encode($this->input->post('hashtags')),
                'status'  => is_null($this->input->post('status')) ? 1 : 0
            ];

            // Update planner in database.
            $this->planner_model->updateByID($plannerID, $plannerData);

            // If failed to create planner redirect to create planner page and display
            // error message.
            if ($this->db->affected_rows() != '1') {
                $this->session->set_flashdata('error', 'Failed to update planner.' );

                redirect('/planner/' . $plannerID . '/update', 'refresh');
                return;
            }

            // Add current logged in user in co buddies  data.
            $coBuddiesData[] = [
                'planner_id' => $plannerID,
                'user_id' => $loggedInUser->id,
            ];

            // Add other co buddy from co_buddy inputs into co buddies data.
            foreach ($coBuddiesID as $key => $coBuddyID) {
                $coBuddiesData[] = [
                    'planner_id' => $plannerID,
                    'user_id' => $coBuddyID,
                ];
            }

            $this->user_planner_model->deleteByPlannerID($plannerID);

            $this->user_planner_model->createMultiple($coBuddiesData);

            // If failed to create planner buddy redirect to create planner page and display
            // error message.
            if ($this->db->affected_rows() < 1) {
                $this->session->set_flashdata('error', 'Failed to update planner co buddies.' );

                redirect('/planner/' . $plannerID . '/update', 'refresh');
                return;
            }

            $this->session->set_flashdata('message', 'Planner successfully update.' );

            redirect('/planner/' . $plannerID . '/update', 'refresh');

        } else {
            // Load page to create planner.
            $data['title'] = 'Update Planner';

            $this->load->view('layouts/header', $data);
            $this->load->view('layouts/sidebar');
            $this->load->view('pages/planner/update', $data);
            $this->load->view('layouts/footer');
        }
    }

    /**
     * Delete planner from database.
     * Delete by planner ID. Allow user to delete planner owned by the user only.
     * Cannot delete the planner if the user not own the planner. This means the
     * user only co buddy for the planner.
     */
    public function delete()
    {
        $plannerID = $this->input->post('planner_id');

        $userID = $this->ion_auth->user()->row()->id;

        if ($plannerID) {
            $result = $this->planner_model->deleteByIDandUserID($plannerID, $userID);

            header('Content-Type: application/json');

            if ($result) {
                echo true;
                return;
            }

            echo false;
            return;
        }
    }

    /**
     * Set pagination element
     *
     * @param $baseURL
     * @param $total
     * @param $perPage
     * @param $uriSegment
     * @return mixed
     */
    protected function setPagination($baseURL, $total, $perPage, $uriSegment)
    {
        $this->load->config('pagination');
        $this->config->config['pagination']['total_rows'] = $total;
        $choice = $total / $perPage;
        $this->config->config['pagination']["num_links"] = 9;
        $this->config->config['pagination']["per_page"] = $perPage;
        $this->config->config['pagination']['base_url'] = $baseURL;
        $this->config->config['pagination']['use_page_numbers'] = true;
        $this->config->config['pagination']['uri_segment'] = $uriSegment;
        $this->config->config['pagination']['next_link'] = 'Next';
        $this->config->config['pagination']['prev_link'] = 'Previous';
        //$this->config->config['pagination']['first_url'] = $this->config->config['pagination']['base_url'].'?'.http_build_query($_GET);

        if (count($_GET) > 0) $this->config->config['pagination']['suffix'] = '?' . http_build_query($_GET, '', "&");

        $this->pagination->initialize($this->config->config['pagination']);

        return $this->pagination->create_links();
    }
}