<?php

class Tag extends MY_Controller
{

    /**
     * Planner constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
        $this->load->library('form_validation');
        $this->load->model('tag_model');
        $this->load->model('tag_file_model');
        $this->load->library('pagination');
    }

    /**
     * List all tags has been created by logged in user.
     */
    public function index($offset = 0)
    {
        (isset($_GET['limit']) ? $limit = $_GET['limit'] : $limit = 10);

        $loggedInUser = $this->ion_auth->user()->row();

        $data['total_tags'] = $this->tag_model->countByUserID($loggedInUser->id);
        $data['tags'] = $this->tag_model->getTagsByUserID($loggedInUser->id, $limit, $offset);
        $data['pagination'] = $this->setPagination(base_url() . "tag/", $data['total_tags'],
            $limit, $urlSegment = 2);
        $data['limit'] = $limit;

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $data['total_tags'];
        }

        foreach ($data['tags'] as $key => $tag) {
            $data['tags'][$key]['files'] = $this->tag_file_model->getByTagID($tag['id']);
        }

        $data['title'] = 'Tag List';

        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/sidebar');
        $this->load->view('pages/tag/index');
        $this->load->view('layouts/footer');

    }

    /**
     * Display form to create new tag for logged in  user.
     */
    public function create()
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('location_name', 'location name', 'required', ['required' => 'Tag location is required.']);
        $this->form_validation->set_rules('latitude', 'latitude', 'required', ['required' => 'Tag location is required.']);
        $this->form_validation->set_rules('longitude', 'longitude', 'required', ['required' => 'Tag location is required.']);

        if ($this->form_validation->run() === TRUE) {
            $uploadedTagFiles = $this->uploadTagFileIfExist();

            $loggedInUser = $this->ion_auth->user()->row();

            $data = [
                'user_id' => $loggedInUser->id,
                'title' => $this->input->post('title'),
                'description'  => $this->input->post('description'),
                'location_name'    => $this->input->post('location_name'),
                'location_image_url' => $this->input->post('location_image_url'),
                'latitude'    => $this->input->post('latitude'),
                'longitude'    => $this->input->post('longitude'),
            ];

            $this->tag_model->create($data);

            $insertID = $this->db->insert_id();

            if ($this->db->affected_rows() != '1') {
                $this->session->set_flashdata('error', 'Failed to create new tag.' );

                redirect('/tag/create', 'refresh');
                return;
            }

            if (!empty($uploadedTagFiles)) {
                foreach ($uploadedTagFiles as $key => $uploadedTagFile) {
                    $data = [
                        'tag_id' => $insertID,
                        'filename' => $uploadedTagFile['file_name'],
                        'type' => $uploadedTagFile['file_type'],
                        'extension' => $uploadedTagFile['file_ext'],
                        'size' => $uploadedTagFile['file_size'],
                    ];

                    $this->tag_file_model->create($data);

                    if ($this->db->affected_rows() != '1') {
                        // redirect them back to the admin page if admin, or to the base url if non admin
                        $this->session->set_flashdata('error', 'Failed to create new tag.' );

                        redirect('/tag/create', 'refresh');
                        return;
                    }
                }
            }

            $this->session->set_flashdata('message', 'Successfully created new tag.' );

            redirect('/tag', 'refresh');

        } else {
            $data['title'] = 'Create New Tag';

            $this->load->view('layouts/header', $data);
            $this->load->view('layouts/sidebar');
            $this->load->view('pages/tag/create');
            $this->load->view('layouts/footer');
        }
    }

    /**
     * Update tag by tag ID.
     *
     * @param $tagID
     */
    public function update($tagID)
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('location_name', 'location name', 'required', ['required' => 'Tag location is required.']);
        $this->form_validation->set_rules('latitude', 'latitude', 'required', ['required' => 'Tag location is required.']);
        $this->form_validation->set_rules('longitude', 'longitude', 'required', ['required' => 'Tag location is required.']);

        $data['user'] = $this->ion_auth->user()->row();
        $data['tag'] = $this->tag_model->getByID($tagID);
        $data['tag_files'] = $this->tag_file_model->getByTagID($tagID);

        if ($data['user']->id != $data['tag']->user_id) {
            $this->session->set_flashdata('error', 'You cannot edit other people tag.' );

            redirect('/tag/index', 'refresh');
            return;
        }

        if ($this->form_validation->run() === TRUE) {
            $uploadedTagFiles = $this->uploadTagFileIfExist();

            $loggedInUser = $this->ion_auth->user()->row();

            $data = [
                'user_id' => $loggedInUser->id,
                'title' => $this->input->post('title'),
                'description'  => $this->input->post('description'),
                'location_name'    => $this->input->post('location_name'),
                'location_image_url' => $this->input->post('location_image_url'),
                'latitude'    => $this->input->post('latitude'),
                'longitude'    => $this->input->post('longitude'),
            ];

            $result = $this->tag_model->updateByID($tagID, $data);

            if (!$result) {
                $this->session->set_flashdata('error', 'Failed to update tag.' );

                redirect('/tag/' . $tagID . '/update', 'refresh');
                return;
            }

            if (!empty($uploadedTagFiles)) {
                foreach ($uploadedTagFiles as $key => $uploadedTagFile) {
                    $data = [
                        'tag_id' => $tagID,
                        'filename' => $uploadedTagFile['file_name'],
                        'type' => $uploadedTagFile['file_type'],
                        'extension' => $uploadedTagFile['file_ext'],
                        'size' => $uploadedTagFile['file_size'],
                    ];

                    $this->tag_file_model->create($data);

                    if ($this->db->affected_rows() != '1') {
                        // redirect them back to the admin page if admin, or to the base url if non admin
                        $this->session->set_flashdata('error', 'Failed to update tag.' );

                        redirect('/tag/' . $tagID . '/update', 'refresh');
                        return;
                    }
                }
            }

            $this->session->set_flashdata('message', 'Tag Successfully update.' );

            redirect('/tag/' . $tagID . '/update', 'refresh');

        } else {
            $data['title'] = 'Update Tag';

            $this->load->view('layouts/header', $data);
            $this->load->view('layouts/sidebar');
            $this->load->view('pages/tag/update', $data);
            $this->load->view('layouts/footer');
        }
    }

    /**
     * Delete tag from database.
     * Delete by tag ID.
     */
    public function delete()
    {
        $tagID = $this->input->post('tag_id');

        $userID =  $this->ion_auth->user()->row()->id;

        if ($tagID) {
            $result = $this->tag_model->deleteByIDandUserID($tagID, $userID);

            header('Content-Type: application/json');

            if ($result) {
                echo true;
                return;
            }

            echo false;
            return;
        }

        header('Content-Type: application/json');
        echo false;
        return;
    }

    /**
     * Upload tag files
     */
    public function uploadTagFileIfExist()
    {
        if (!empty($_FILES['tag_files']['name'])) {
            $this->initializeUploadLibrary();

            $totalFile = count($_FILES['tag_files']['name']);

            $uploadedFilesInfo = [];

            for ($i = 0; $i < $totalFile; $i++) {
                if (!empty($_FILES['tag_files']['name'][$i])) {
                    $_FILES['userfile']['name']     = $_FILES['tag_files']['name'][$i];
                    $_FILES['userfile']['type']     = $_FILES['tag_files']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $_FILES['tag_files']['tmp_name'][$i];
                    $_FILES['userfile']['error']    = $_FILES['tag_files']['error'][$i];
                    $_FILES['userfile']['size']     = $_FILES['tag_files']['size'][$i];

                    if ( ! $this->upload->do_upload()) {
                        $error = array('error' => $this->upload->display_errors());

                        $this->session->set_flashdata('error',$error);

                        redirect(base_url( 'tag/create'), 'refresh');
                        return;
                    } else {
                        $uploadedFilesInfo[$i] = $this->upload->data();

                        $fileNameSuffix = uniqid();

                        $newFilename = $uploadedFilesInfo[$i]['raw_name'] . '_' . uniqid() . $uploadedFilesInfo[$i]['file_ext'];

                        rename($uploadedFilesInfo[$i]['full_path'], $uploadedFilesInfo[$i]['file_path'] . $newFilename);

                        $uploadedFilesInfo[$i]['file_name'] = $newFilename;
                        $uploadedFilesInfo[$i]['full_path'] = $uploadedFilesInfo[$i]['file_path'] . $newFilename;
                        $uploadedFilesInfo[$i]['raw_name'] = $uploadedFilesInfo[$i]['raw_name'] . '_' . $fileNameSuffix;
                        $uploadedFilesInfo[$i]['orig_name'] = $newFilename;
                        $uploadedFilesInfo[$i]['client_name'] = $newFilename;

                    }
                }
            }

            return $uploadedFilesInfo;
        }

        echo '';
    }

    public function autoCompleteOwnTags()
    {
        $userID =  $this->ion_auth->user()->row()->id;

        $keyword = $this->input->get('keyword');

        $ownTags = $this->tag_model->autoCompleteOwnTagsTitle($keyword, $userID);

        header('Content-Type: application/json');
        echo json_encode($ownTags);
    }

    public function searchOwnTags()
    {
        $inputName = $this->input->get('keyword');

        $normalUsers = $this->tag_model->searchOwnTagsByTitleAndDescription($inputName);

        header('Content-Type: application/json');
        echo json_encode($normalUsers);
    }

    /**
     * Initialize upload library with specific configuration.
     */
    protected function initializeUploadLibrary()
    {
        $config['upload_path']   = './uploads/tag_files/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|xls|xlsx|csv|mp3|mp4|mpg|mpeg|wmv';
        $config['max_size']      = 20000;
        $config['max_width']     = 1920;
        $config['max_height']    = 1024;

        $this->load->library('upload', $config);
    }

    /**
     * Set pagination element
     *
     * @param $baseURL
     * @param $total
     * @param $perPage
     * @param $uriSegment
     * @return mixed
     */
    protected function setPagination($baseURL, $total, $perPage, $uriSegment)
    {
        $this->load->config('pagination');
        $this->config->config['pagination']['total_rows'] = $total;
        $choice = $total / $perPage;
        $this->config->config['pagination']["num_links"] = 9;
        $this->config->config['pagination']["per_page"] = $perPage;
        $this->config->config['pagination']['base_url'] = $baseURL;
        $this->config->config['pagination']['use_page_numbers'] = true;
        $this->config->config['pagination']['uri_segment'] = $uriSegment;
        $this->config->config['pagination']['next_link'] = 'Next';
        $this->config->config['pagination']['prev_link'] = 'Previous';
        //$this->config->config['pagination']['first_url'] = $this->config->config['pagination']['base_url'].'?'.http_build_query($_GET);

        if (count($_GET) > 0) $this->config->config['pagination']['suffix'] = '?' . http_build_query($_GET, '', "&");

        $this->pagination->initialize($this->config->config['pagination']);

        return $this->pagination->create_links();
    }

}