<?php

class Auth extends MY_Controller
{

    /**
     * Auth constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
    }
    /**
     * Authenticate User using ion auth library
     *
     */
    public function login()
    {
        $this->redirectUserToDashboardIfLoggedIn();

        // Set validation rule for email and password.
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'Login';

            $this->load->view('auth/login', $data);
            return;
        }

        $this->processLogin();
    }

    /**
     * Logout user from application.
     */
    public function logout()
    {
        $this->ion_auth->logout();
        redirect(base_url() . 'auth/login', 'refresh');
    }

    /**
     * Process login after user click submit button on login form.
     */
    protected function processLogin()
    {
        // If validation passed then authenticate user.
        if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), (bool) $this->input->post('remember'))) {
            redirect(base_url( 'dashboard'), 'refresh');
            return;
        }

        $this->session->set_flashdata('error',$this->ion_auth->errors());

        redirect(base_url( 'auth/login'), 'refresh');
    }

    // forgot password
    public function forgotPassword()
    {
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');

        if ($this->form_validation->run() == false) {
            header('Content-Type: application/json');
            echo json_encode(['error' => validation_errors()]);
        } else {
            $user = $this->ion_auth->where('email', $this->input->post('email'))->users()->row();

            if(empty($user)) {
                header('Content-Type: application/json');
                echo json_encode(['error' => 'Email not existed.']);
                return;
            }

            // run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($user->email);

            if ($forgotten) {
                header('Content-Type: application/json');
                echo json_encode(['message' => $this->ion_auth->messages()]);
                return;
            } else {
                header('Content-Type: application/json');
                echo json_encode(['error' => $this->ion_auth->errors()]);
                return;
            }
        }
    }

    public function resetPassword($code = NULL)
    {
        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            $this->form_validation->set_rules('new_password', 'new password', 'required|min_length[6]|max_length[20]|matches[confirm_new_password]');
            $this->form_validation->set_rules('confirm_new_password', 'confirm new password', 'required');

            if ($this->form_validation->run() == false)
            {
                $data['title'] = 'Reset Password';
                $data['user_id'] = $user->id;
                $data['code'] = $code;
                $this->load->view('auth/reset_password', $data);
            } else {
                if ($user->id != $this->input->post('user_id')) {
                    $this->ion_auth->clear_forgotten_password_code($code);

                    show_error($this->lang->line('error_csrf'));
                } else {
                    $email = $user->email;

                    $change = $this->ion_auth->reset_password($email, $this->input->post('new_password'));

                    if ($change) {
                        // if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect("auth/login", 'refresh');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->errors());
                        redirect('auth/reset_password/' . $code, 'refresh');
                    }
                }
            }
        } else {
            show_404();
        }
    }

}