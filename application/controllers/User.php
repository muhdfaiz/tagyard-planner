<?php

class User extends MY_Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
        $this->load->library('form_validation');
        $this->load->model('user_model');
    }

    public function searchNormalUsersForAutoComplete()
    {
        $inputName = $this->input->get('name');

        $normalUsers = $this->user_model->searchNormalUsers($inputName);

        header('Content-Type: application/json');
        echo json_encode($normalUsers);
    }

    public function edit()
    {
        $this->form_validation->set_rules('first_name', 'first name', 'required');
        $this->form_validation->set_rules('last_name', 'last name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');

        $loggedInUser = $this->ion_auth->user()->row();

        // update the password if it was posted
        if ($this->input->post('password'))
        {
            $this->form_validation->set_rules('password', 'new password', 'min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
            $this->form_validation->set_rules('confirm_new_password', 'confirm new password', '');
        }

        if ($this->form_validation->run() === TRUE) {
            $uploadedAvatar = $this->uploadAvatarIfExist();

            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'email'    => $this->input->post('email'),
            );

            // update the password if it was posted
            if ($this->input->post('password')) {
                $data['password'] = $this->input->post('password');
            }

            if (!empty($uploadedAvatar)) {
                $data['avatar'] = $uploadedAvatar['upload_data']['file_name'];
            }

            if($this->ion_auth->update($loggedInUser->id, $data)) {
                if (!empty($uploadedAvatar)) {
                    unlink(FCPATH . '/uploads/' . $loggedInUser->avatar);
                }

                $this->session->set_flashdata('message', $this->ion_auth->messages() );

                redirect('user/edit', 'refresh');
                return;
            } else {
                // redirect them back to the admin page if admin, or to the base url if non admin
                $this->session->set_flashdata('error', $this->ion_auth->errors() );

                redirect('user/edit', 'refresh');
            }
        } else {
            $data['title'] = 'User';
            $data['user'] = $loggedInUser;

            $this->load->view('layouts/header', $data);
            $this->load->view('layouts/sidebar');
            $this->load->view('pages/user/update', $data);
            $this->load->view('layouts/footer');
        }

    }

    protected function initializeUploadLibrary()
    {
        $config['upload_path']   = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = 1000;
        $config['max_width']     = 1024;
        $config['max_height']    = 768;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
    }

    protected function uploadAvatarIfExist()
    {
        if (!empty($_FILES['avatar']['name'])) {
            $this->initializeUploadLibrary();

            if ( ! $this->upload->do_upload('avatar')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('error',$error);

                redirect(base_url( 'auth/login'), 'refresh');
            } else {
                $data = array('upload_data' => $this->upload->data());

                return $data;
            }
        }

        return '';
    }

}