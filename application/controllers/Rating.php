<?php

class Rating extends MY_Controller
{
    /**
     * Rating constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectUserToLoginPageIfNotLogin();
        $this->load->model('tag_model');
        $this->load->model('planner_model');
        $this->load->model('tag_rating_model');
        $this->load->model('planner_rating_model');
    }

    /**
     * Create tag rating and store in database.
     *
     * @param $plannerID
     * @param $tagID
     */
    public function createTagRating($plannerID, $tagID)
    {
        $ratingValue = $this->input->post('value');

        if (!empty($ratingValue)) {
            // Check if planner ID exist. If not exist return an error.
            $planner = $this->planner_model->getByID($plannerID);

            if (empty($planner)) {
                header('Content-Type: application/json');

                $errorMessage = ['error' => 'Planner does not exist.'];
                echo json_encode($errorMessage);
                return;
            }

            // Check if tag ID exist. If not exist return an error.
            $tag = $this->tag_model->getByID($tagID);

            if (empty($tag)) {
                header('Content-Type: application/json');

                $errorMessage = ['error' => 'Tag does not exist.'];
                echo json_encode($errorMessage);
                return;
            }

            $loggedInUser = $this->ion_auth->user()->row();

            $this->tag_rating_model->deleteByUserIDAndPlannerIDAndTagID($loggedInUser->id, $plannerID, $tagID);

            // Set tag rating data
            $rating = [
                'user_id' => $loggedInUser->id,
                'planner_id' => $plannerID,
                'tag_id' => $tagID,
                'value' => $ratingValue,
            ];

            $this->tag_rating_model->create($rating);

            if ($this->db->affected_rows() == '1') {
                header('Content-Type: application/json');

                echo true;
                return;
            }

            header('Content-Type: application/json');

            echo false;
            return;
        }

        header('Content-Type: application/json');

        echo false;
        return;
    }

    /**
     * Delete tag rating by planner ID and tag ID for specific user.
     *
     * @param $plannerID
     * @param $tagID
     */
    public function deleteTagRating($plannerID, $tagID)
    {
        // Check if planner ID exist. If not exist return an error.
        $planner = $this->planner_model->getByID($plannerID);

        if (empty($planner)) {
            header('Content-Type: application/json');

            $errorMessage = ['error' => 'Planner does not exist.'];
            echo json_encode($errorMessage);
            return;
        }

        // Check if tag ID exist. If not exist return an error.
        $tag = $this->tag_model->getByID($tagID);

        if (empty($tag)) {
            header('Content-Type: application/json');

            $errorMessage = ['error' => 'Tag does not exist.'];
            echo json_encode($errorMessage);
            return;
        }

        $loggedInUser = $this->ion_auth->user()->row();

        $result = $this->tag_rating_model->deleteByUserIDAndPlannerIDAndTagID($loggedInUser->id, $plannerID, $tagID);

        if ($result) {
            header('Content-Type: application/json');

            echo true;
            return;
        }

        header('Content-Type: application/json');

        echo false;
        return;
    }

    /**
     * Create new planner rating.
     * For example when co buddy want to give rating for certain planner.
     *
     * @param $plannerID
     */
    public function createPlannerRating($plannerID)
    {
        $ratingValue = $this->input->post('value');

        if (!empty($ratingValue)) {
            // Check if planner ID exist. If not exist return an error.
            $planner = $this->planner_model->getByID($plannerID);

            if (empty($planner)) {
                header('Content-Type: application/json');

                $errorMessage = ['error' => 'Planner does not exist.'];
                echo json_encode($errorMessage);
                return;
            }

            $loggedInUser = $this->ion_auth->user()->row();

            $this->planner_rating_model->deleteByUserIDAndPlannerID($loggedInUser->id, $plannerID);

            // Set planner rating data
            $rating = [
                'user_id' => $loggedInUser->id,
                'planner_id' => $plannerID,
                'value' => $ratingValue,
            ];

            $this->planner_rating_model->create($rating);

            if ($this->db->affected_rows() == '1') {
                header('Content-Type: application/json');

                echo true;
                return;
            }

            header('Content-Type: application/json');

            echo false;
            return;
        }

        header('Content-Type: application/json');

        echo false;
        return;
    }

    /**
     * Delete planner rating by Planner ID for specific user.
     *
     * @param $plannerID
     */
    public function deletePlannerRating($plannerID)
    {
        // Check if planner ID exist. If not exist return an error.
        $planner = $this->planner_model->getByID($plannerID);

        if (empty($planner)) {
            header('Content-Type: application/json');

            $errorMessage = ['error' => 'Planner does not exist.'];
            echo json_encode($errorMessage);
            return;
        }

        $loggedInUser = $this->ion_auth->user()->row();

        $result = $this->planner_rating_model->deleteByUserIDAndPlannerID($loggedInUser->id, $plannerID);

        if ($result) {
            header('Content-Type: application/json');

            echo true;
            return;
        }

        header('Content-Type: application/json');

        echo false;
        return;
    }

}