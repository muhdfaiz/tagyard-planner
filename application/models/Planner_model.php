<?php

class Planner_model extends CI_Model
{
    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * Tag_Image_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'planners';
    }

    /**
     * Create new tag file and store in database.
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $this->db->insert($this->table, $data);
    }

    /**
     * Update planner by ID.
     *
     * @param $plannerID
     * @param $data
     * @return mixed
     */
    public function updateByID($plannerID, $data)
    {
        return $this->db->set($data)->where('id', $plannerID)->update($this->table);
    }

    /**
     * Delete planner by ID and user ID.
     *
     * @param $userID
     * @param $tagID
     */
    public function deleteByIDandUserID($plannerID, $userID)
    {
        return $this->db->where('id', $plannerID)->where('user_id', $userID)->delete($this->table);
    }
    /**
     * Retrieve planner by ID.
     *
     * @param $plannerID
     * @return mixed
     */
    public function getByID($plannerID)
    {
        $query = $this->db->where('id', $plannerID)->order_by('id', 'desc')->limit(1, 0)
            ->get($this->table);

        return $query->row();
    }

    /**
     * Retrieve planner co buddies.
     *
     * @param $loggedInUserID
     * @return mixed
     */
    public function getPlannerCoBuddies($plannerID, $loggedInUserID)
    {
        $query = $this->db->select("*, users.id as value, CONCAT(first_name, ' ', last_name) as fullname")->from('users_planners')
            ->join('users', 'users.id = users_planners.user_id', 'inner')
            ->join('planners', 'planners.id = users_planners.planner_id', 'inner')
            ->where('users_planners.user_id !=', $loggedInUserID)
            ->where('users_planners.planner_id', $plannerID)->get();

        return $query->result_array();
    }


}