<?php

class User_model extends CI_Model
{
    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * User_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'users';
    }

    /**
     * Retrieve user by user ID.
     *
     * @param $userID
     * @return mixed
     */
    public function getByID($userID)
    {
        $query = $this->db->where('id', $userID)->get($this->table);

        return $query->row();
    }

    /**
     * Get normal users from database. Normal user means user that sit in group members.
     * Refer groups table.
     */
    public function searchNormalUsers($inputName)
    {
        if (!empty($inputName)) {
            $explodedName = explode(' ', $inputName);

            $this->db->select("users.id as value, CONCAT(first_name, ' ', last_name) as fullname")->from('users_groups')
                ->join('users', 'users.id = users_groups.user_id', 'inner')
                ->join('groups', 'groups.id = users_groups.group_id', 'inner');

            if (count($explodedName) == 1) {
                $query = $this->db->like('first_name', $explodedName[0])
                    ->or_like('last_name', $explodedName[0])
                    ->where('group_id', 2)->get();

                return $query->result_array();
            }

            if (count($explodedName) == 2) {
                $query = $this->db->like('first_name', $explodedName[0])
                    ->like('last_name', $explodedName[1])
                    ->where('group_id', 2)->get();

                return $query->result_array();
            }

        }

        $query = $this->db->where('group_id', 2)->get();

        return $query->result_array();
    }

}