<?php

class Tag_remark_model extends CI_Model
{

    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * Tag remark model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'tag_remarks';
    }

    /**
     * Create new tag remark and store in database.
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $this->db->insert($this->table, $data);
    }

    /**
     * Retrieve tag remark by ID.
     *
     * @param $tagRemarkID
     * @return mixed
     */
    public function getByID($tagRemarkID)
    {
        $query = $this->db->where('id', $tagRemarkID)->order_by('id', 'asc')->limit(1, 0)
            ->get($this->table);

        return $query->row();
    }

    /**
     * Retrieve tag remarks by planner id and tag id.
     *
     * @param $plannerID
     * @param $tagID
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getByPlannerIDAndTagID($plannerID, $tagID, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $query = $this->db->join('users', 'tag_remarks.user_id = users.id', 'left')
            ->where('planner_id', $plannerID)->where('tag_id', $tagID)
            ->order_by('tag_remarks.id', 'asc')->limit($limit, $offset)
            ->get($this->table);

        return $query->result_array();
    }

}