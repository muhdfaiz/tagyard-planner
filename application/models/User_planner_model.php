<?php

class User_planner_model extends CI_Model
{
    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * User_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'users_planners';
    }

    /**
     * Insert planner buddy in database.
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $this->db->insert($this->table, $data);
    }

    /**
     * Delete user planner by planner ID.
     *
     * @param $plannerID
     */
    public function deleteByPlannerID($plannerID)
    {
        return $this->db->where('planner_id', $plannerID)->delete($this->table);
    }

    /**
     * Insert multiple planner buddy in database.
     *
     * @param $data
     */
    public function createMultiple($data)
    {
        $this->db->insert_batch($this->table, $data);
    }

    /**
     * Retrieve user planner by Planner ID.
     *
     * @param $plannerID
     * @return mixed
     */
    public function getByPlannerID($plannerID)
    {
        $query = $this->db->select("*")
            ->from($this->table)
            ->join('planners', 'planners.id = users_planners.planner_id', 'inner')
            ->join('users', 'users.id = users_planners.user_id', 'inner')
            ->where('users_planners.planner_id', $plannerID)->get();

        return $query->row();
    }

    /**
     * Retrieve user planner by user ID.
     *
     * @param $userID
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getByUserID($userID, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $query = $this->db->select("*")
            ->from($this->table)
            ->join('planners', 'planners.id = users_planners.planner_id', 'inner')
            ->join('users', 'users.id = users_planners.user_id', 'inner')
            ->where('users_planners.user_id', $userID)->order_by('users_planners.id', 'desc')->limit($limit, $offset)
            ->get();

        return $query->result_array();
    }

    /**
     * Count total planner by user ID.
     *
     * @param $userID
     * @return mixed
     */
    public function countByUserID($userID)
    {
        return $this->db->from($this->table)->where('user_id', $userID)->count_all_results();
    }
}