<?php

class Tag_file_model extends CI_Model
{
    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * Tag_Image_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'tag_files';
    }

    /**
     * Create new tag file and store in database.
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->db->insert($this->table, $data);
    }

    /**
     * Get tag file by ID.
     *
     * @param $id
     * @return mixed
     */
    public function getByID($id)
    {
        $query = $this->db->where('id', $id)->get($this->table);

        return $query->row();
    }

    /**
     * Get tag file by filename.
     *
     * @param $id
     * @return mixed
     */
    public function getByFilename($fileName)
    {
        $query = $this->db->where('filename', $fileName)->get($this->table);

        return $query->row();
    }


    /**
     * Retrieve tag file by tag ID.
     *
     * @param $tagID
     * @return mixed
     */
    public function getByTagID($tagID)
    {
        $query = $this->db->where('tag_id', $tagID)->get($this->table);

        return $query->result_array();
    }

    /**
     * Delete tag file by ID.
     *
     * @param $tagFileID
     * @return mixed
     */
    public function deleteByID($tagFileID)
    {
        return $this->db->where('id', $tagFileID)->delete($this->table);
    }

    /**
     * Delete taf file by tag ID.
     *
     * @param $tagID
     * @return mixed
     */
    public function deleteByTagID($tagID)
    {
        return $this->db->where('tag_id', $tagID)->delete($this->table);
    }

    /**
     * Delete tag file by filename.
     *
     * @param $filename
     * @return mixed
     */
    public function deleteByFilename($filename)
    {
        return $this->db->where('filename', $filename)->delete($this->table);
    }
}