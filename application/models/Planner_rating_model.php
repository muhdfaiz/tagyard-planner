<?php

class Planner_rating_model extends CI_Model
{

    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * Planner rating model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'planner_ratings';
    }

    /**
     * Create new planner rating and store in database.
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $this->db->insert($this->table, $data);
    }

    /**
     * Delete planner rating by user ID and planner ID.
     *
     * @param $userID
     * @param $plannerID
     */
    public function deleteByUserIDAndPlannerID($userID, $plannerID)
    {
        return $this->db->where('user_id', $userID)->where('planner_id', $plannerID)
            ->delete($this->table);
    }

    /**
     * Retrieve planner rating by ID.
     *
     * @param $tagRatingID
     * @return mixed
     */
    public function getByID($tagRatingID)
    {
        $query = $this->db->where('id', $tagRatingID)->order_by('id', 'asc')->limit(1, 0)
            ->get($this->table);

        return $query->row();
    }

    /**
     * Retrieve planner rating by user ID, planner id
     *
     * @param $plannerID
     * @param $tagID
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getByUserIDAndPlannerID($userID, $plannerID, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $query = $this->db->where('user_id', $userID)->where('planner_id', $plannerID)
            ->order_by('id', 'asc')->limit($limit, $offset)
            ->get($this->table);

        return $query->result_array();
    }

}