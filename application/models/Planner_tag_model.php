<?php


class Planner_tag_model extends CI_Model
{
    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * Tag_Image_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'planners_tags';
    }

    /**
     * Assign multiple tags to planner in database.
     *
     * @param $data
     * @return mixed
     */
    public function createMultiple($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    /**
     * Retrieve tag_planner by planner ID.
     *
     * @param $tagID
     * @return mixed
     */
    public function getByPlannerID($plannerID)
    {
        $query = $this->db->select("*")
            ->from($this->table)
            ->join('planners', 'planners.id = planners_tags.planner_id', 'inner')
            ->join('tags', 'tags.id = planners_tags.tag_id', 'inner')
            ->where('planners_tags.planner_id', $plannerID)->order_by("planners_tags.order", "asc")->get();

        return $query->result_array();
    }

    /**
     * Delete by planner ID.
     *
     * @param $plannerID
     * @return mixed
     */
    public function deleteByPlannerID($plannerID)
    {
        return $this->db->where('planner_id', $plannerID)->delete($this->table);
    }

    /**
     * Delete by tag ID.
     *
     * @param $plannerID
     * @return mixed
     */
    public function deleteByTagID($tagID)
    {
        return $this->db->where('tag_id', $tagID)->delete($this->table);
    }

}