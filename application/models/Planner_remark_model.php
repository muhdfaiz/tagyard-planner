<?php

class Planner_remark_model extends CI_Model
{

    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * Planner remark model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'planner_remarks';
    }

    /**
     * Create new tag remark and store in database.
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $this->db->insert($this->table, $data);
    }

    /**
     * Retrieve planner remark by ID.
     *
     * @param $plannerRemarkID
     * @return mixed
     */
    public function getByID($plannerRemarkID)
    {
        $query = $this->db->where('id', $plannerRemarkID)->order_by('id', 'asc')->limit(1, 0)
            ->get($this->table);

        return $query->row();
    }

    /**
     * Retrieve planner remarks by planner id.
     *
     * @param $plannerID
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getByPlannerID($plannerID, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $query = $this->db->join('users', 'planner_remarks.user_id = users.id', 'left')
            ->where('planner_id', $plannerID)
            ->order_by('planner_remarks.id', 'asc')->limit($limit, $offset)
            ->get($this->table);

        return $query->result_array();
    }

}