<?php

class Tag_model extends CI_Model
{
    /**
     * Table Name
     *
     * @var $table
     */
    protected $table;

    /**
     * Tag_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'tags';
    }

    /**
     * Create new tag and store in database.
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $this->db->insert($this->table, $data);
    }

    /**
     * Update tag by ID.
     *
     * @param $tagID
     * @param $data
     * @return mixed
     */
    public function updateByID($tagID, $data)
    {
        return $this->db->set($data)->where('id', $tagID)->update($this->table);
    }


    /**
     * Delete tag by ID and user ID.
     *
     * @param $userID
     * @param $tagID
     */
    public function deleteByIDandUserID($tagID, $userID)
    {
        return $this->db->where('id', $tagID)->where('user_id', $userID)->delete($this->table);
    }

    /**
     * Retrieve tag by ID.
     *
     * @param $tagID
     * @return mixed
     */
    public function getByID($tagID)
    {
        $query = $this->db->where('id', $tagID)->order_by('id', 'desc')->limit(1, 0)
            ->get($this->table);

        return $query->row();
    }

    /**
     * Retrieve tags by user ID.
     *
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getTagsByUserID($userID, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $query = $this->db->where('user_id', $userID)->order_by('id', 'desc')->limit($limit, $offset)
            ->get($this->table);

        return $query->result_array();
    }

    /**
     * Retrieve tags by user ID.
     *
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getAllTags($limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $query = $this->db->order_by('id', 'desc')->limit($limit, $offset)
            ->get($this->table);

        return $query->result_array();
    }

    /**
     * Retrieve tags by user ID and tags that not assign to current Planner..
     *
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getTagsNotAssignToPlannerByUserID($userID, $plannerID, $keyword, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $this->db->select("tags.*, planners_tags.tag_id, planners_tags.planner_id")
            ->from($this->table)
            ->join('planners_tags', 'tags.id = planners_tags.tag_id', 'left')
            ->where('tags.user_id', $userID)->order_by('tags.id', 'desc');

        if (!empty($keyword)) {
            $query = $this->db->like('tags.title', $keyword)->where('planners_tags.tag_id IS NULL')
                ->or_where('planners_tags.planner_id !=', $plannerID)
                ->limit($limit, $offset)->get();
        } else {
            $query = $this->db->where('planners_tags.tag_id IS NULL')
                ->or_where('planners_tags.planner_id !=', $plannerID)
                ->limit($limit, $offset)->get();
        }
    
        return $query->result_array();
    }

    public function getOtherTagsNotAssignToPlanner($userID, $plannerID, $keyword, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $this->db->select("tags.*, planners_tags.tag_id, planners_tags.planner_id")
            ->from($this->table)
            ->join('planners_tags', 'tags.id = planners_tags.tag_id', 'left')
            ->where('tags.user_id !=', $userID)->order_by('tags.id', 'desc');

        if (!empty($keyword)) {
            $query = $this->db->like('tags.title', $keyword)->where('planners_tags.tag_id IS NULL')
                ->or_where('planners_tags.planner_id !=', $plannerID)
                ->limit($limit, $offset)->get();
        } else {
            $query = $this->db->where('planners_tags.tag_id IS NULL')
                ->or_where('planners_tags.planner_id !=', $plannerID)
                ->limit($limit, $offset)->get();
        }

        return $query->result_array();
    }

    /**
     * Search own tags by title and description.
     *
     * @param $keyword
     * @return mixed
     */
    public function searchOwnTagsByTitleAndDescription($keyword)
    {
        if (!empty($keyword)) {
            $query = $this->db->select()->like('title', $keyword)->or_like('description', $keyword)->get($this->table);

            return $query->result_array();
        }
    }

    /**
     * Search own tags by title use in auto complete.
     *
     * @param $keyword
     * @return mixed
     */
    public function autoCompleteOwnTagsTitle($keyword, $userID)
    {
        if (!empty($keyword)) {
            $query = $this->db->select("title")->like('title', $keyword)->where('user_id', $userID)->get($this->table);

            return $query->result_array();
        }
    }

    /**
     * Count total tag by user ID.
     *
     * @param $userID
     * @return mixed
     */
    public function countByUserID($userID)
    {
        return $this->db->from($this->table)->where('user_id', $userID)->count_all_results();
    }

}