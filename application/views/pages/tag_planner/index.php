<!-- MAIN CONTENT
      ================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

            <h3 class="page-header">
                Manage Planner Tags
            </h3>

        </div>
    </div> <!-- / .row -->

    <br>

    <div class="planner-info row">
        <input type="hidden" name="planner_id" value="<?php echo $planner['planner_id']; ?>">
        <div class="col-xs-2">
            <dl>
                <dt>Planner name:</dt>
                <dd><?php echo $planner['name']; ?></dd>
            </dl>
        </div>
        <div class="col-xs-4">
            <dl>
                <dt>Planner Hashtags:</dt>
                <dd>
                    <?php if (!empty($planner['hashtags'])) { ?>
                        <?php $hashtags = json_decode($planner['hashtags'], true); ?>

                        <?php foreach ($hashtags as $key => $hashtag) { ?>
                            <span class="label label-info"><?php echo $hashtag; ?></span>
                        <?php } ?>
                    <?php } ?>
                </dd>
            </dl>
        </div>
        <div class="col-xs-3">
            <dl>
                <dt>Planner Status:</dt>
                <dd>
                    <?php if ($planner['status'] == '0') { ?>
                        <span class="label label-danger">Private</span>
                    <?php } ?>

                    <?php if ($planner['status'] == '1') { ?>
                        <span class="label label-danger">Public</span>
                    <?php } ?>
                </dd>
            </dl>
        </div>
        <div class="col-xs-3">
            <dl>
                <dt>Planner Co-Buddy:</dt>
                <dd>

                    <?php if (!empty($planner['co_buddy'])) { ?>
                        <?php foreach ($planner['co_buddy'] as $key => $coBuddy) { ?>
                            <div class="co_buddy_avatar" data-toggle="tooltip" title="<?php echo $coBuddy['first_name'] . ' ' . $coBuddy['last_name'] ?>">
                                <?php if (!empty($coBuddy['avatar'])) { ?>
                                    <img class="avatar" src="<?php echo base_url('uploads/' . $coBuddy['avatar']); ?>"/>
                                <?php } else { ?>
                                    <img class="avatar" src="<?php echo base_url('/assets/img/default-avatar.png'); ?>"/>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </dd>
            </dl>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-4">
            <ul class="nav nav-justified nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">Drag Tag Here</a>
                </li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab_1">
                    <div id="panel-tag-item" class="panel panel-default">
                        <div class="panel-body">
                            <div id="tag-container" class="col-xs-12">
                                <?php foreach ($tags as $tag) { ?>
                                    <div class="tag-item col-xs-12" data-tag-id="<?php echo $tag['id']; ?>">
                                        <div class="tag-block" style="background-size: 100% 100%; background-image: url('<?php echo $tag['location_image_url']; ?>');">
                                            <div class="tag-content">
                                                <div class="tag-title">
                                                    <?php echo $tag['title']; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-7 col-xs-offset-1">
            <div class="search-tag-container">
                <div class="search-tag-title">
                    SEARCH TAGS
                </div>
                <div class="search-tag-body">
                    <div class="input-group">
                        <input type="text" class="form-control" id="search-tag-input" name="keyword" value="<?php echo $keyword; ?>" placeholder="Keywords">
                        <div class="input-group-btn">
                            <button id="search-tag-button" class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <ul style="clear: both;" id="tag-tabs" class="nav nav-justified nav-tabs" role="tablist">
                <li role="presentation" id="own-tags-tab" class="active">
                    <a href="#search_tab_1" aria-controls="search_tab_1" role="tab" data-toggle="tab">Your Tags</a>
                </li>
                <li role="presentation" id="other-tags-tab">
                    <a href="#search_tab_2" aria-controls="search_tab_2" role="tab" data-toggle="tab">Others Tag</a>
                </li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="search_tab_1">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div id="own-tag-container" class="col-xs-12">
                                <?php foreach ($own_tags as $key => $own_tag) { ?>
                                    <div class="col-xs-6 tag-item" data-tag-id="<?php echo $own_tag['id']; ?>">
                                        <div class="tag-block" style="background-size: 100% 100%; background-image: url('<?php echo $own_tag['location_image_url']; ?>');">
                                            <div class="tag-content">
                                                <div class="tag-title">
                                                    <?php echo $own_tag['title']; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="search_tab_2">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div id="other-tag-container" class="col-xs-12">
                                <?php foreach ($other_tags as $key => $other_tag) { ?>
                                    <div class="col-xs-6 tag-item" data-tag-id="<?php echo $other_tag['id']; ?>">
                                        <div class="tag-block" style="background-size: 100% 100%; background-image: url('<?php echo $other_tag['location_image_url']; ?>');">
                                            <div class="tag-content">
                                                <div class="tag-title">
                                                    <?php echo $other_tag['title']; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- / .row -->
</div> <!-- / .container-fluid -->