<!-- MAIN CONTENT
      ================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

            <h3 class="page-header">
                Create New Planner
            </h3>

        </div>
    </div> <!-- / .row -->
    <div class="row">
        <div class="col-xs-12 col-sm-9">
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>

            <?php if ($this->session->flashdata('message')) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Successful!</strong> <?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>

           <?php echo form_open(base_url('planner/create'), ['class' => 'form-horizontal', 'id' => 'create-planner-form']); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Planner info</h4>
                </div>
                <div class="panel-body">
                    <?php if (form_error('name')) { ?>
                        <div class="form-group has-error">
                    <?php } else { ?>
                        <div class="form-group">
                    <?php } ?>

                        <label class="col-sm-2 control-label" for="name">Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" id="name" class="form-control" value="<?php echo set_value('name'); ?>">

                            <?php if (form_error('name')) {
                                echo form_error('name', '<span id="helpBlock4" class="help-block">', '</span>');
                            } ?>
                        </div>
                    </div>

                    <?php if (form_error('hashtags[]')) { ?>
                        <div class="form-group has-error">
                    <?php } else { ?>
                        <div class="form-group">
                    <?php } ?>

                        <label class="col-sm-2 control-label" for="hashtags[]">Hashtags</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="hashtags[]" id="hashtags" multiple>
                                <?php if (!empty(set_value('hashtags'))) { ?>
                                    <?php foreach (set_value('hashtags') as $key => $hashtag) { ?>
                                        <option value="<?php echo $hashtag; ?>"><?php echo $hashtag; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>

                            <?php if (form_error('hashtags[]')) {
                                echo form_error('hashtags[]', '<span id="helpBlock4" class="help-block">', '</span>');
                            } ?>
                        </div>
                    </div>

                    <?php if (form_error('co_buddy')) { ?>
                        <div class="form-group has-error">
                    <?php } else { ?>
                        <div class="form-group">
                    <?php } ?>

                        <label class="col-sm-2 control-label" for="co_buddy">Co Buddy Name</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="co_buddy[]" id="co_buddy" multiple>
                                <?php if (!empty(set_value('co_buddy'))) { ?>
                                    <?php foreach (set_value('co_buddy') as $key => $coBuddy) { ?>
                                        <option value="<?php echo $coBuddy; ?>"><?php echo $coBuddy; ?></option>
                                    <?php }  ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="status">Status</label>
                        <div class="col-sm-10">
                            <input type="checkbox" name="status" id="planner_status" data-size="small" data-on-text="Private" data-off-text="Public" value="0" checked>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">CREATE PLANNER</button>
                        </div>
                    </div>

                </div>
            <?php echo form_close(); ?>
        </div>
    </div> <!-- / .row -->

    <div class="col-xs-12 col-sm-3">

        <div>
            <div class="form-info">
                <div class="form-info-icon">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                </div>
                <div class="form-info-body" ">
                    <h4>
                        Insert Hashtag
                    </h4>
                    Type hashtag you want to insert in hashtag input and press enter after you finish.
                </div>
            </div>

            <div class="form-info">
                <div class="form-info-icon">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                </div>
                <div class="form-info-body" ">
                    <h4>
                        Search Co Buddy
                    </h4>
                    Enter user name you want to search inside co buddy name text input and select the user appear.
                </div>
            </div>
        </div>

    </div>
</div> <!-- / .container-fluid -->