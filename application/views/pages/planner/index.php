<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

            <h3 class="page-header">
                List Of Planners
            </h3>

            <!-- Display Error Message -->
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>

            <!-- Display Success Message -->
            <?php if ($this->session->flashdata('message')) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Successful!</strong> <?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <!-- Bordered table -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="<?php echo base_url('planner/create'); ?>" class="btn btn-primary btn-sm">Create New Planner</a>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>ID</th>
                            <th>Owner</th>
                            <th>Name</th>
                            <th>Hashtags</th>
                            <th>Co-Buddy</th>
                            <th>Status</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($planners as $planner) { ?>
                            <tr>
                                <td>
                                    <a class="update-user" href="<?php echo base_url() . 'planner/' . $planner['planner_id'] . '/update'; ?>">
                                        <div class="btn btn-info btn-xs">Update</div>
                                    </a>
                                    <a class="delete-planner" data-id="<?php echo $planner['id'] ; ?>" data-name="<?php echo $planner['name'] ; ?>">
                                        <div class="btn btn-danger btn-xs">Delete</div>
                                    </a>
                                    <a class="manage-tag" href="<?php echo base_url() . 'planner/' . $planner['planner_id'] . '/tag'; ?>">
                                        <div class="btn btn-primary btn-xs">Manage Tags</div>
                                    </a>
                                    <a class="manage-tag" href="<?php echo base_url() . 'planner/' . $planner['planner_id'] . '/review'; ?>">
                                        <div class="btn btn-warning btn-xs">Review</div>
                                    </a>
                                </td>
                                <td><?php echo $planner['id'] ; ?></td>
                                <td>
                                    <?php if ($user->email == $planner['email']) { ?>
                                        <?php echo 'You' ; ?>
                                    <?php } else { ?>
                                        <?php echo $planner['first_name'] . ' ' . $planner['last_name'] ; ?>
                                    <?php } ?>
                                </td>
                                <td><?php echo $planner['name'] ; ?></td>
                                <td>
                                    <?php $hashtags = json_decode($planner['hashtags'], true); ?>

                                    <?php if (!empty($hashtags)) { ?>
                                        <ul>
                                            <?php foreach ($hashtags as $key => $hashtag) { ?>
                                                <li style="margin: 5px 0;"><span class="label label-info"><?php echo $hashtag; ?></span></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if (!empty($planner['co_buddy'])) { ?>
                                        <ul>
                                            <?php foreach ($planner['co_buddy'] as $key => $coBuddy) { ?>
                                                <li style="margin: 5px 0;"><?php echo $coBuddy['fullname']; ?></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if ($planner['status'] == 0) { ?>
                                        <span class="label label-danger">Public</span>
                                    <?php } else { ?>
                                        <span class="label label-info">Private</span>

                                    <?php } ?>

                                </td>
                                <td><?php echo $planner['created_at'] ; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                    <!-- Render Pagination -->
                    <div class="row">
                        <div class="col-sm-5">
                            <?php if (isset($pagination_message)) { ?>
                                <div class="pagination-message"><?php echo $pagination_message; ?></div>
                            <?php } ?>
                        </div>
                        <div class="col-sm-7">
                            <div class="pagination-links">
                                <?php echo $pagination; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>