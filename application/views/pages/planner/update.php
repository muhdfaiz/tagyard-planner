<!-- MAIN CONTENT
      ================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

            <h3 class="page-header">
                Update Planner
            </h3>

        </div>
    </div> <!-- / .row -->
    <div class="row">
        <div class="col-xs-12 col-sm-9">
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>

            <?php if ($this->session->flashdata('message')) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Successful!</strong> <?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>

            <?php echo form_open(base_url('planner/' . $planner->id . '/update'), ['class' => 'form-horizontal', 'id' => 'update-planner-form']); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Planner info</h4>
                </div>
                <div class="panel-body">
                    <?php if (form_error('name')) { ?>
                    <div class="form-group has-error">
                        <?php } else { ?>
                        <div class="form-group">
                            <?php } ?>

                            <label class="col-sm-2 control-label" for="name">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" id="name" class="form-control" value="<?php echo !empty(set_value('name')) ? set_value('name') : $planner->name  ?>">

                                <?php if (form_error('name')) {
                                    echo form_error('name', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>
                        </div>

                        <?php if (form_error('hashtags[]')) { ?>
                        <div class="form-group has-error">
                            <?php } else { ?>
                            <div class="form-group">
                                <?php } ?>

                                <label class="col-sm-2 control-label" for="hashtags[]">Hashtags</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="hashtags[]" id="hashtags" multiple>
                                        <?php if (!empty($planner->hashtags)) { ?>
                                            <?php $hashTags = json_decode($planner->hashtags, true); ?>

                                            <?php foreach ($hashTags as $key => $hashTag) { ?>
                                                <option value="<?php echo $hashTag; ?>"><?php echo $hashTag; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>

                                    <?php if (form_error('hashtags[]')) {
                                        echo form_error('hashtags[]', '<span id="helpBlock4" class="help-block">', '</span>');
                                    } ?>
                                </div>
                            </div>

                            <?php if (form_error('co_buddy')) { ?>
                            <div class="form-group has-error">
                                <?php } else { ?>
                                <div class="form-group">
                                    <?php } ?>

                                    <label class="col-sm-2 control-label" for="co_buddy">Co Buddy Name</label>
                                    <div class="col-sm-10">
                                        <select style="display: none;" class="form-control" name="current_co_buddy" id="current_co_buddy" multiple>
                                            <?php if (!empty($planner_buddies)) { ?>
                                                <?php foreach ($planner_buddies as $key => $plannerBuddy) { ?>
                                                    <option value="<?php echo $plannerBuddy['value']; ?>"><?php echo $plannerBuddy['fullname']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <select class="form-control" name="co_buddy[]" id="co_buddy" multiple></select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="status">Status</label>
                                    <div class="col-sm-10">

                                        <?php if ($planner->status == '0') { ?>
                                            <input type="checkbox" name="status" id="planner_status" data-size="small" data-on-text="Private" data-off-text="Public" value="0" checked>
                                        <?php } ?>

                                        <?php if ($planner->status == '1') { ?>
                                            <input type="checkbox" name="status" id="planner_status" data-size="small" data-on-text="Private" data-off-text="Public" value="1">
                                        <?php } ?>

                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <button type="submit" class="btn btn-primary">UPDATE PLANNER</button>
                                    </div>
                                </div>

                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div> <!-- / .row -->

                    <div class="col-xs-12 col-sm-3">

                        <div>
                            <div class="form-info">
                                <div class="form-info-icon">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </div>
                                <div class="form-info-body" ">
                                <h4>
                                    Insert Hashtag
                                </h4>
                                Type hashtag you want to insert in hashtag input and press enter after you finish.
                            </div>
                        </div>

                        <div class="form-info">
                            <div class="form-info-icon">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                            </div>
                            <div class="form-info-body" ">
                            <h4>
                                Search Co Buddy
                            </h4>
                            Enter user name you want to search inside co buddy name text input and select the user appear.
                        </div>
                    </div>
                </div>

            </div>
        </div> <!-- / .container-fluid -->