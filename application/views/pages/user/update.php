<!-- MAIN CONTENT
      ================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

            <h3 class="page-header">
                Edit profile
                <small>Account</small>
            </h3>

        </div>
    </div> <!-- / .row -->
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>

            <?php if ($this->session->flashdata('message')) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Successful!</strong> <?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>

            <?php echo form_open_multipart(base_url('user/edit'), ['class' => 'form-horizontal']); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Avatar</h4>
                    </div>
                    <div class="panel-body">
                            <div class="profile__avatar">
                                <?php if (!empty($user->avatar)) { ?>
                                    <img class="profile-image" src="<?php echo base_url('uploads/' . $user->avatar); ?>"/>
                                <?php } else { ?>
                                    <img class="profile-image" src="<?php echo base_url('/assets/img/default-avatar.png'); ?>"/>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="avatar">Upload avatar</label>
                                <input type="file" name="avatar" class="upload-button">
                            </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">User info</h4>
                    </div>
                    <div class="panel-body">
                        <?php if (form_error('first_name')) { ?>
                         <div class="form-group has-error">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>

                            <label class="col-sm-2 control-label" for="first_name">Your firstname</label>
                            <div class="col-sm-10">
                                <input type="text" name="first_name" id="first_name" class="form-control" value="<?php echo !empty(set_value('first_name')) ? set_value('first_name') : $user->first_name  ?>">

                                <?php if (form_error('first_name')) {
                                    echo form_error('first_name', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>
                        </div>

                        <?php if (form_error('last_name')) { ?>
                            <div class="form-group has-error">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>

                            <label class="col-sm-2 control-label" for="last_name">Your lastname</label>
                            <div class="col-sm-10">
                                <input type="text" name="last_name" id="last_name" class="form-control" value="<?php echo !empty(set_value('last_name')) ? set_value('last_name') : $user->last_name ?>">

                                <?php if (form_error('last_name')) {
                                    echo form_error('last_name', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>

                        </div>

                        <?php if (form_error('email')) { ?>
                            <div class="form-group has-error">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>

                            <label class="col-sm-2 control-label" for="email">Your e-mail</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo !empty(set_value('email')) ? set_value('email') : $user->email ?>">

                                <?php if (form_error('email')) {
                                    echo form_error('email', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Password</h4>
                    </div>
                    <div class="panel-body">
                        <?php if (form_error('password')) { ?>
                            <div class="form-group has-error">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>

                            <label class="col-sm-2 control-label" for="password">Your new password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" id="password" class="form-control">

                                <?php if (form_error('password')) {
                                    echo form_error('password', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>

                        </div>

                        <?php if (form_error('confirm_new_password')) { ?>
                            <div class="form-group has-error">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>

                            <label class="col-sm-2 control-label" for="confirm_new_password">Confirm your new password</label>
                            <div class="col-sm-10">
                                <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control">

                                <?php if (form_error('confirm_new_password')) {
                                    echo form_error('confirm_new_password', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-link">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php echo form_close(); ?>

        </div>
    </div> <!-- / .row -->


</div> <!-- / .container-fluid -->