<!-- MAIN CONTENT
      ================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

            <h3 class="page-header">
                Planner Review
            </h3>

        </div>
    </div> <!-- / .row -->

    <br>
    <div class="planner-info row">
        <input type="hidden" name="planner_id" value="<?php echo $planner['planner_id']; ?>">
        <div class="col-xs-2">
            <dl>
                <dt>Planner name:</dt>
                <dd><?php echo $planner['name']; ?></dd>
            </dl>
        </div>
        <div class="col-xs-4">
            <dl>
                <dt>Planner Hashtags:</dt>
                <dd>
                    <?php if (!empty($planner['hashtags'])) { ?>
                        <?php $hashtags = json_decode($planner['hashtags'], true); ?>

                        <?php foreach ($hashtags as $key => $hashtag) { ?>
                            <span class="label label-info"><?php echo $hashtag; ?></span>
                        <?php } ?>
                    <?php } ?>
                </dd>
            </dl>
        </div>
        <div class="col-xs-3">
            <dl>
                <dt>Planner Status:</dt>
                <dd>
                    <?php if ($planner['status'] == '0') { ?>
                        <span class="label label-danger">Private</span>
                    <?php } ?>

                    <?php if ($planner['status'] == '1') { ?>
                        <span class="label label-danger">Public</span>
                    <?php } ?>
                </dd>
            </dl>
        </div>
        <div class="col-xs-3">
            <dl>
                <dt>Planner Co-Buddy:</dt>
                <dd>

                    <?php if (!empty($planner['co_buddy'])) { ?>
                        <?php foreach ($planner['co_buddy'] as $key => $coBuddy) { ?>
                            <div class="co_buddy_avatar" data-toggle="tooltip" title="<?php echo $coBuddy['first_name'] . ' ' . $coBuddy['last_name'] ?>">
                                <?php if (!empty($coBuddy['avatar'])) { ?>
                                    <img class="avatar" src="<?php echo base_url('uploads/' . $coBuddy['avatar']); ?>"/>
                                <?php } else { ?>
<<<<<<< Updated upstream
                                    <img class="avatar" src="<?php echo base_url('/assets/img/default-avatar.png'); ?>"/>
=======
                                    <img class="avatar" src="/assets/img/default-avatar.png"/>
>>>>>>> Stashed changes
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </dd>
            </dl>
        </div>
    </div>

    <div class="planner-review row">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="col-xs-12">
                    <div class="tag-group-list col-xs-12">
                        <ul id="tag-list" class="nav tabs-left">
                            <?php foreach ($tagGroups as $key => $tagGroup) { ?>
                                <?php if ($key == 0) { ?>
                                    <li class="active" data-tag-group-index="<?php echo $key; ?>">
                                        <a class="tagGroupTab" href="#tab<?php echo $key; ?>" data-tag-group-index="<?php echo $key; ?>" data-toggle="tab">
                                            <div class="tag-group current col-xs-12">
                                            <div class="title">
                                                <?php echo $tagGroup['0']['location_name'] . ' TO ' . $tagGroup['1']['location_name'] ?>
                                            </div>
                                <?php } else { ?>
                                    <li data-tag-group-index="<?php echo $key; ?>">
                                        <a class="tagGroupTab" href="#tab<?php echo $key; ?>" data-tag-group-index="<?php echo $key; ?>" data-toggle="tab">
                                            <div class="tag-group col-xs-12">
                                            <div class="title">
                                                <?php echo $tagGroup['0']['location_name'] . ' TO ' . $tagGroup['1']['location_name'] ?>
                                            </div>
                                <?php } ?>
                                        <div class="images" data-origin-latitude="<?php echo $tagGroup['0']['latitude']; ?>"
                                             data-origin-longitude="<?php echo $tagGroup['0']['longitude']; ?>"
                                             data-destination-latitude="<?php echo $tagGroup['1']['latitude']; ?>"
                                             data-destination-longitude="<?php echo $tagGroup['1']['longitude']; ?>">

                                            <img class="first-image" src="<?php echo $tagGroup['0']['location_image_url']; ?>"/>
                                            <img class="second-image" src="<?php echo $tagGroup['1']['location_image_url']; ?>"/>
                                        </div>
                                    </div>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-9 content">
                <div class="tab-content">
            <?php foreach ($tagGroups as $key => $tagGroup) { ?>
                <?php if ($key == 0) { ?>
                    <div class="tab-pane active" id="tab<?php echo $key; ?>">
                <?php } else { ?>
                    <div class="tab-pane" id="tab<?php echo $key; ?>">
                <?php } ?>

                    <!-- Tabs -->
                    <ul class="nav nav-justified nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tag-review<?php echo $key; ?>" class="tag-group-detail" aria-controls="tag-review<?php echo $key; ?>" role="tab" data-toggle="tab">Tag Review</a>
                        </li>
                        <li role="presentation">
                            <a href="#start-tag<?php echo $key; ?>" class="tag-group-detail" aria-controls="start-tag<?php echo $key; ?>" role="tab" data-toggle="tab">Start Tag</a>
                        </li>
                        <li role="presentation">
                            <a href="#end-tag<?php echo $key; ?>" class="tag-group-detail" aria-controls="end-tag<?php echo $key; ?>" role="tab" data-toggle="tab">End Tag</a>
                        </li>
                        <li role="presentation">
                            <a href="#summary<?php echo $key; ?>" data-summary-index="<?php echo $key; ?>" data-name="summary-detail" class="tag-group-detail" aria-controls="summary<?php echo $key; ?>" role="tab" data-toggle="tab">Summary</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tag-review<?php echo $key; ?>">
                            <div class="row m-lr-0 summary-description">
                                <div class="col-xs-12">
                                    <ul class="timeline">
<<<<<<< Updated upstream

=======
>>>>>>> Stashed changes
                                        <!-- Empty even item -->
                                        <li></li>

                                        <!-- Events -->
                                        <li class="event">
                                            <div class="event__title">
                                                <h4>End Tag</h4>
                                                <p><b><?php echo $tagGroup[1]['location_name']; ?></b> (<?php echo $tagGroup[1]['latitude']; ?>, <?php echo $tagGroup[1]['longitude']; ?>)</p>
                                            </div>
                                            <div class="event__content">
                                                <p><?php echo $tagGroup[1]['description']; ?></p>
                                            </div>
                                        </li>
                                        <li class="event">
                                            <div class="event__title">
                                                <h4>Start Tag</h4>
                                                <p><b><?php echo $tagGroup[0]['location_name']; ?></b> (<?php echo $tagGroup[0]['latitude']; ?>, <?php echo $tagGroup[0]['longitude']; ?>)</p>

                                            </div>
                                            <div class="event__content">
                                                <p><?php echo $tagGroup[0]['description']; ?></p>
                                            </div>
                                        </li>

                                        <!-- Clearfix -->
                                        <li class="clearfix"></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row m-lr-0">
                                <div class="col-xs-12" style="display: inline-block;">
                                    <h4>ETA:</h4>
                                    <h4 id="eta<?php echo $key; ?>"></h4>
                                </div>
                            </div>

                            <div class="row m-lr-0">
>>>>>>> Stashed changes
                                <div class="col-xs-12">
                                    <div id="tagReviewMap<?php echo $key; ?>" class="map"></div>
                                </div>
                            </div>


                        </div>

                        <div role="tabpanel" class="tab-pane" id="start-tag<?php echo $key; ?>">
                            <!-- Tag Files -->
                            <?php if (!empty($tagGroup[0]['files'])) { ?>
                                <div class="row m-lr-0" style="margin-top: 20px;">
                                    <div class="col-xs-12">
                                        <b>List Of Files</b>
                                    </div>
                                </div>
                                <div class="row m-lr-0 tag-files">
                                    <?php foreach ($tagGroup[0]['files'] as $startTagFile) { ?>
                                        <?php if (in_array($startTagFile['extension'], ['.jpeg', '.jpg', '.png', '.gif'])) { ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <a target="_blank" href="<?php echo base_url('uploads/tag_files/' . $startTagFile['filename']); ?>">
                                                    <div class="thumbnail">
                                                        <img src="<?php echo base_url('uploads/tag_files/' . $startTagFile['filename']); ?>" alt="...">
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } else if (in_array($startTagFile['extension'], ['.mp4', '.avi', '.mpg', '.mpeg', '.mp3'])) { ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <a target="_blank" href="<?php echo base_url('uploads/tag_files/' . $startTagFile['filename']); ?>">
                                                    <div class="video-thumbnail">
                                                        <p><?php echo $startTagFile['extension']; ?></p>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } else if (in_array($startTagFile['extension'], ['.doc', '.docx', '.xls', '.xlsx', '.txt', '.pdf', '.csv'])) { ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <a target="_blank" href="<?php echo base_url('uploads/tag_files/' . $startTagFile['filename']); ?>">
                                                    <div class="doc-thumbnail">
                                                        <p><?php echo $startTagFile['extension']; ?></p>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                            <!-- Tag Rating -->
                            <div class="row m-lr-0 tag-rating-container">
                                <div class="col-xs-12">
                                    <label class="control-label">Rate This Tag</label>

                                    <?php if (isset($userTagRatings[$tagGroup[0]['id']])) { ?>
                                        <input class="start-tag-rating rating rating-loading" value="<?php echo $userTagRatings[$tagGroup[0]['id']]['value']; ?>"
                                               data-tag-id="<?php echo $tagGroup[0]['id']; ?>" data-planner-id="<?php echo $plannerID; ?>" data-min="0"
                                               data-max="5" data-step="1" data-size="xs">
                                    <?php } else { ?>
                                        <input name="input-1" class="end-tag-rating rating rating-loading" data-tag-id="<?php echo $tagGroup[0]['id']; ?>"
                                               data-planner-id="<?php echo $plannerID; ?>" data-min="0" data-max="5" data-step="1" data-size="xs">
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- Co Buddy Remark -->
                            <div class="row m-lr-0 co-buddy-remark">
                                <div class="col-xs-12">
                                    <div class="comment-wrapper">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                Remark From Co Buddy
                                            </div>
                                            <div class="panel-body">
                                                <textarea class="form-control" name="tag_remark<?php echo $tagGroup[0]['id']; ?>"
                                                          placeholder="Write your remark..." rows="3" data-tag-id=""></textarea>
                                                <br>

                                                <button type="button" class="submit-tag-remark btn btn-accent pull-right" data-planner-id="<?php echo $plannerID; ?>"
                                                        data-tag-id="<?php echo $tagGroup[0]['id']; ?>">Submit</button>

                                                <div class="clearfix"></div>
                                                <hr>

                                                <ul class="media-list tag-remark-container<?php echo $tagGroup[0]['id']; ?>">
                                                    <?php foreach ($tagRemarks[$tagGroup[0]['id']] as $key1 => $tagRemark) { ?>
                                                        <li class="media">
                                                            <a href="#" class="pull-left">
                                                                <?php if (!empty($tagRemark['avatar'])) { ?>
                                                                    <img src="<?php echo base_url('uploads/' . $tagRemark['avatar']); ?>" alt="" class="img-circle">
                                                                <?php } else { ?>
<<<<<<< Updated upstream
                                                                    <img src="<?php echo base_url('/assets/img/default-avatar.png'); ?>" alt="" class="img-circle">
=======
                                                                    <img src="/assets/img/default-avatar.png" alt="" class="img-circle">
>>>>>>> Stashed changes
                                                                <?php } ?>
                                                            </a>
                                                            <div class="media-body">
                                                            <span class="text-muted pull-right">
                                                                <small class="text-muted"><?php echo $tagRemark['created_at']; ?></small>
                                                            </span>
                                                                <strong class="text-success"><?php echo $tagRemark['first_name'] . ' ' . $tagRemark['last_name']; ?></strong>
                                                                <p><?php echo $tagRemark['content']; ?></p>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="end-tag<?php echo $key; ?>">
                            <!-- Tag Files -->
                            <?php if (!empty($tagGroup[1]['files'])) { ?>
                                <div class="row m-lr-0" style="margin-top: 20px;">
                                    <div class="col-xs-12">
                                        <b>List Of Files</b>
                                    </div>
                                </div>

                                <div class="row m-lr-0 tag-files">
                                    <?php foreach ($tagGroup[1]['files'] as $endTagFile) { ?>
                                        <?php if (in_array($endTagFile['extension'], ['.jpeg', '.jpg', '.png', '.gif'])) { ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail">
                                                    <img src="<?php echo base_url('uploads/tag_files/' . $endTagFile['filename']); ?>" alt="...">
                                                </div>
                                            </div>
                                        <?php } else if (in_array($endTagFile['extension'], ['.mp4', '.avi', '.mpg', '.mpeg', '.mp3'])) { ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <a target="_blank" href="<?php echo base_url('uploads/tag_files/' . $endTagFile['filename']); ?>">
                                                    <div class="video-thumbnail">
                                                        <p><?php echo $endTagFile['extension']; ?></p>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } else if (in_array($endTagFile['extension'], ['.doc', '.docx', '.xls', '.xlsx', '.txt', '.pdf', '.csv'])) { ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <a target="_blank" href="<?php echo base_url('uploads/tag_files/' . $endTagFile['filename']); ?>">
                                                    <div class="video-thumbnail">
                                                        <p><?php echo $endTagFile['extension']; ?></p>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                            <!-- Tag  Rating -->
                            <div class="row m-lr-0 tag-rating-container">
                                <div class="col-xs-12">
                                    <label class="control-label">Rate This Tag</label>
                                    <?php if (isset($userTagRatings[$tagGroup[1]['id']])) { ?>
                                        <input name="input-1" class="end-tag-rating rating rating-loading" value="<?php echo $userTagRatings[$tagGroup[1]['id']]['value']; ?>" data-tag-id="<?php echo $tagGroup[1]['id']; ?>"
                                               data-planner-id="<?php echo $plannerID; ?>" data-min="0" data-max="5" data-step="1" data-size="xs">
                                    <?php } else { ?>
                                        <input name="input-1" class="end-tag-rating rating rating-loading" data-tag-id="<?php echo $tagGroup[1]['id']; ?>"
                                               data-planner-id="<?php echo $plannerID; ?>" data-min="0" data-max="5" data-step="1" data-size="xs">
                                    <?php } ?>
                                </div>
                            </div>


                            <!-- Co Buddy Remark -->
                            <div class="row m-lr-0 co-buddy-remark">
                                <div class="col-xs-12">
                                    <div class="comment-wrapper">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                Remark From Co Buddy
                                            </div>
                                            <div class="panel-body">
                                                <textarea class="form-control" name="tag_remark<?php echo $tagGroup[1]['id']; ?>"
                                                          placeholder="Write your remark..." rows="3" data-tag-id=""></textarea>
                                                <br>

                                                <button type="button" class="submit-tag-remark btn btn-accent pull-right" data-planner-id="<?php echo $plannerID; ?>"
                                                        data-tag-id="<?php echo $tagGroup[1]['id']; ?>">Submit</button>

                                                <div class="clearfix"></div>
                                                <hr>

                                                <ul class="media-list tag-remark-container<?php echo $tagGroup[1]['id']; ?>">
                                                    <?php foreach ($tagRemarks[$tagGroup[1]['id']] as $key2 => $tagRemark) { ?>
                                                        <li class="media">
                                                            <a href="#" class="pull-left">
                                                                <?php if (!empty($tagRemark['avatar'])) { ?>
                                                                    <img src="<?php echo base_url('uploads/' . $tagRemark['avatar']); ?>" alt="" class="img-circle">
                                                                <?php } else { ?>
<<<<<<< Updated upstream
                                                                    <img src="<?php echo base_url('/assets/img/default-avatar.png'); ?>" alt="" class="img-circle">
=======
                                                                    <img src="/assets/img/default-avatar.png" alt="" class="img-circle">
>>>>>>> Stashed changes
                                                                <?php } ?>
                                                            </a>
                                                            <div class="media-body">
                                                            <span class="text-muted pull-right">
                                                                <small class="text-muted"><?php echo $tagRemark['created_at']; ?></small>
                                                            </span>
                                                                <strong class="text-success"><?php echo $tagRemark['first_name'] . ' ' . $tagRemark['last_name']; ?></strong>
                                                                <p><?php echo $tagRemark['content']; ?></p>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="summary<?php echo $key; ?>">
                            <div class="row m-lr-0">
                                <div class="col-xs-12">
                                    <div id="plannerSummaryMap<?php echo $key; ?>" class="map"></div>
                                </div>
                            </div>

                            <div class="row m-lr-0 summary-description">
<<<<<<< Updated upstream
                                <div class="col-sm-3 col-xs-12">
                                    <ul class="list-group">
                                        <li class="list-group-item"><b>Total Number Of Tags</b> <span class="badge"><?php echo $total_tags; ?></span></li>
=======
                                <div class="col-sm-6 col-xs-12">
                                    <ul class="list-group">
                                        <li class="list-group-item"><b>Total Number Of Tags</b> <span class="badge"><?php echo $total_tags; ?></span></li>
                                        <li class="list-group-item"><b>Total ETA</b><span class="badge"><?php echo $totalETA; ?></span></li>
                                        <li class="list-group-item"><b>Total Distance</b><span class="badge"><?php echo $totalDistance; ?> KM</span></li>
>>>>>>> Stashed changes
                                    </ul>
                                </div>
                            </div>

                            <!-- Planner Rating -->
                            <div class="row m-lr-0 planner-rating-container">
                                <div class="col-xs-12">
                                    <label class="control-label">Rate This Planner</label>
                                    <?php if (!empty($userPlannerRating)) { ?>
                                        <input name="input-1" class="planner-rating rating rating-loading" value="<?php echo $userPlannerRating[0]['value']; ?>"
                                               data-planner-id="<?php echo $plannerID; ?>" data-min="0" data-max="5" data-step="1" data-size="xs">
                                    <?php } else { ?>
                                        <input class="planner-rating rating rating-loading" data-planner-id="<?php echo $plannerID; ?>"
                                               data-min="0" data-max="5" data-step="1" data-size="xs">
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- Co Buddy Remark -->
                            <div class="row m-lr-0 co-buddy-remark">
                                <div class="col-xs-12">
                                    <div class="comment-wrapper">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                Remark From Co Buddy
                                            </div>
                                            <div class="panel-body">
                                                <textarea class="form-control" name="planner_remark<?php echo $plannerID; ?>_<?php echo $key; ?>"
                                                          placeholder="Write your remark..." rows="3"></textarea>
                                                <br>

                                                <button type="button" class="submit-planner-remark btn btn-accent pull-right"
                                                        data-planner-id="<?php echo $plannerID; ?>" data-planner-key="<?php echo $key; ?>">Submit</button>

                                                <div class="clearfix"></div>
                                                <hr>

                                                <ul class="media-list planner-remark-container<?php echo $plannerID; ?>-<?php echo $key; ?>">
                                                    <?php foreach ($plannerRemarks as $key3 => $plannerRemark) { ?>
                                                        <li class="media">
                                                            <a href="#" class="pull-left">
                                                                <?php if (!empty($plannerRemark['avatar'])) { ?>
                                                                    <img src="<?php echo base_url('uploads/' . $plannerRemark['avatar']); ?>" alt="" class="img-circle">
                                                                <?php } else { ?>
<<<<<<< Updated upstream
                                                                    <img src="<?php echo base_url('/assets/img/default-avatar.png'); ?>" alt="" class="img-circle">
=======
                                                                    <img src="/assets/img/default-avatar.png" alt="" class="img-circle">
>>>>>>> Stashed changes
                                                                <?php } ?>
                                                            </a>
                                                            <div class="media-body">
                                                            <span class="text-muted pull-right">
                                                                <small class="text-muted"><?php echo $plannerRemark['created_at']; ?></small>
                                                            </span>
                                                                <strong class="text-success"><?php echo $plannerRemark['first_name'] . ' ' . $plannerRemark['last_name']; ?></strong>
                                                                <p><?php echo $plannerRemark['content']; ?></p>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div> <!-- / .container-fluid -->