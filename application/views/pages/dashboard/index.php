<!-- MAIN CONTENT
================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

            <h1 class="page-header">
                Dashboard <small>Dashboard and statistics</small>
            </h1>

        </div>
    </div> <!-- / .row -->

    <!-- Dashboard: Stats -->
    <div class="row">
        <div class="col-xs-12 col-sm-3">

            <div class="dashboard-stats__item dashboard-stats__item_red">
                <i class="fa fa-comments"></i>
                <h3 class="dashboard-stats__title">
                    <span class="count-to" data-from="0" data-to="1250">0</span>
                    <small>Total Planner</small>
                </h3>
            </div>

        </div>
        <div class="col-xs-12 col-sm-3">

            <div class="dashboard-stats__item dashboard-stats__item_orange">
                <i class="fa fa-globe"></i>
                <h3 class="dashboard-stats__title">
                    <span class="count-to" data-from="0" data-to="5412">0</span>
                    <small>Total Friends</small>
                </h3>
            </div>

        </div>
        <div class="col-xs-12 col-sm-3">

            <div class="dashboard-stats__item dashboard-stats__item_green">
                <i class="fa fa-pie-chart"></i>
                <h3 class="dashboard-stats__title">
                    <span class="count-to" data-from="0" data-to="4155">0</span>
                    <small>Orders</small>
                </h3>
            </div>

        </div>
        <div class="col-xs-12 col-sm-3">

            <div class="dashboard-stats__item dashboard-stats__item_light-blue">
                <i class="fa fa-eur"></i>
                <h3 class="dashboard-stats__title">
                    $<span class="count-to" data-from="0" data-to="105">0</span>K
                    <small>Total profit</small>
                </h3>
            </div>

        </div>
    </div> <!-- / .row -->

</div> <!-- / .container-fluid -->

