<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

            <h3 class="page-header">
                List Of Tags
            </h3>

            <!-- Display Error Message -->
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>

            <!-- Display Success Message -->
            <?php if ($this->session->flashdata('message')) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Successful!</strong> <?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <!-- Bordered table -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="<?php echo base_url('tag/create'); ?>" class="btn btn-primary btn-sm">Create New Tag</a>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>ID</th>
                            <th>Location Image</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Location Name</th>
                            <th>Coordinate</th>
                            <th>Files</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($tags as $tag) { ?>
                            <tr>
                                <td>
                                    <a class="update-user" href="<?php echo base_url() . 'tag/' . $tag['id'] . '/update'; ?>">
                                        <div class="btn btn-info btn-xs">Update</div>
                                    </a>
                                    <a class="delete-tag" data-id="<?php echo $tag['id'] ; ?>" data-name="<?php echo $tag['title'] ; ?>">
                                        <div class="btn btn-danger btn-xs">Delete</div>
                                    </a>
                                </td>
                                <td><?php echo $tag['id'] ; ?></td>
                                <td><img src="<?php echo $tag['location_image_url']; ?>" style="width: 120px;"/></td>
                                <td><?php echo $tag['title'] ; ?></td>
                                <td><?php echo $tag['description'] ; ?></td>
                                <td><?php echo $tag['location_name'] ; ?></td>
                                <td>
                                    <ul>
                                        <li><?php echo $tag['latitude'] ; ?></li>
                                        <li><?php echo $tag['longitude'] ; ?></li>
                                    </ul>
                                </td>
                                <td>
                                    <?php if (!empty($tag['files'])) { ?>
                                        <ul>
                                            <?php foreach ($tag['files'] as $key => $file) { ?>
                                                <li><a target="_blank" href="<?php echo base_url() . '/uploads/tag_files/' . $file['filename']; ?>"><?php echo $file['filename']; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </td>
                                <td><?php echo $tag['created_at'] ; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                    <!-- Render Pagination -->
                    <div class="row">
                        <div class="col-sm-5">
                            <?php if (isset($pagination_message)) { ?>
                                <div class="pagination-message"><?php echo $pagination_message; ?></div>
                            <?php } ?>
                        </div>
                        <div class="col-sm-7">
                            <div class="pagination-links">
                                <?php echo $pagination; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>