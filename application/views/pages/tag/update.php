<!-- MAIN CONTENT
      ================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <h3 class="page-header">
                Update Tag
            </h3>
        </div>
    </div> <!-- / .row -->

    <div class="row">
        <div class="col-xs-12 col-sm-9">
            <!-- Display Error Message -->
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Error!</strong>
                    <?php if (is_array($this->session->flashdata('error'))) { ?>
                        <ul>
                            <?php foreach ($this->session->flashdata('error') as $error) { ?>
                                <li><?php echo $error; ?></li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <?php echo $this->session->flashdata('error'); ?>
                    <?php } ?>
                </div>
            <?php } ?>

            <!-- Display Success Message -->
            <?php if ($this->session->flashdata('message')) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Successful!</strong> <?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>

            <!-- Create New tag Form -->
            <?php echo form_open_multipart('tag/update/' . $tag->id, ['id' => 'create-tag-form', 'class' => 'form-horizontal']); ?>
            <input type="hidden" name="location_image_url" value="<?php echo $tag->location_image_url; ?>">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Tag info</h4>
                    </div>
                    <div class="panel-body">

                        <!-- Tag Title Input -->
                        <?php if (form_error('title')) { ?>
                            <div class="form-group has-error">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>

                            <label class="col-sm-2 control-label" for="title">Title *</label>
                            <div class="col-sm-9">
                                <input type="text" name="title" id="title" class="form-control" value="<?php echo !empty(set_value('title')) ? set_value('title') : $tag->title  ?>">

                                <?php if (form_error('title')) {
                                    echo form_error('title', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>
                        </div>

                        <!-- Tag Description Input -->
                        <?php if (form_error('description')) { ?>
                            <div class="form-group has-error">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>

                            <label class="col-sm-2 control-label" for="description">Description  *</label>
                            <div class="col-sm-9">
                                <input type="text" name="description" id="description" class="form-control" value="<?php echo !empty(set_value('description')) ? set_value('description') : $tag->description  ?>">

                                <?php if (form_error('description')) {
                                    echo form_error('description', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>
                        </div>

                        <!-- Search Location Map -->
                        <?php if (form_error('location_name') || form_error('longitude') || form_error('latitude')) { ?>
                            <div class="form-group has-error">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>

                            <label class="col-sm-2 control-label" for="location_name">Search Location  *</label>
                            <div class="col-sm-9">
                                <?php if (form_error('location_name') || form_error('longitude') || form_error('latitude')) {
                                    echo form_error('location_name', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                                <input id="search-tag-location" class="controls" type="text" placeholder="Search Tag Location">
                                <div id="tag-map"></div>
                            </div>
                        </div>

                        <!-- Tag Location Name Input -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="location_name">Location Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="location_name" id="location_name" class="form-control" value="<?php echo !empty(set_value('location_name')) ? set_value('location_name') : $tag->location_name  ?>" readonly="readonly">
                            </div>
                        </div>

                        <!-- Tag Latitude Input -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="latitude">Location Latitude</label>
                            <div class="col-sm-9">
                                <input type="text" name="latitude" id="latitude" class="form-control" value="<?php echo !empty(set_value('latitude')) ? set_value('latitude') : $tag->latitude  ?>" readonly="readonly">
                            </div>
                        </div>

                        <!-- Tag Longitude Input -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="longitude">Location Longitude</label>
                            <div class="col-sm-9">
                                <input type="text" name="longitude" id="longitude" class="form-control" value="<?php echo !empty(set_value('longitude')) ? set_value('longitude') : $tag->longitude  ?>" readonly="readonly">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Upload Tag Files</h4>
                    </div>
                    <div class="panel-body">
                        <?php foreach ($tag_files as $tagFile) { ?>
                            <div class="uploaded-tag-file" data-filename="<?php echo $tagFile['filename']; ?>" data-filetype="<?php echo $tagFile['type']; ?>" data-filesize="<?php echo $tagFile['size'] ?>"></div>
                        <?php } ?>
                        <input type="file" id="update-tag-files-input" name="tag_files[]">

                        <!-- Submit Button -->
                        <div class="form-group">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary">UPDATE TAG</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>

        <div class="col-xs-12 col-sm-3">
            <div>
                <div class="form-info">
                    <div class="form-info-icon">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <div class="form-info-body" ">
                        <h4>
                            How To Search Location
                        </h4>
                        <ul style="padding: 0;">
                            <li>
                                Enter the location name you want to search inside the textbox in google maps and select the location from
                                the search result.
                            </li>
                            <br>
                            <li>
                                New marker will be added inside the map that point o the location you select.
                            </li>
                            <br>
                            <li>
                                Location name, latitude and longitude automatically fill when you select the location from search result.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- / .row -->
</div> <!-- / .container-fluid -->