
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    <!-- CSS Plugins -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css'); ?>">

    <!-- CSS Global -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/theme.css'); ?>">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
    <script>var base_url = '<?php echo base_url() ?>';</script>

</head>

<body>


<!-- MAIN CONTENT
================================================== -->
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

            <!-- Sign In -->
            <div class="sign__container">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Sign In to your account
                        </h4>
                    </div> <!-- / .panel-heading -->

                    <div class="panel-body">
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                            </div>
                        <?php } ?>

                        <?php echo form_open(base_url('auth/register')); ?>
                            <?php if (form_error('first_name')) { ?>
                                 <div class="form-group has-error">
                            <?php } else { ?>
                                <div class="form-group">
                            <?php } ?>

                                <label for="first_name">Your firstname</label>
                                <input type="text" name="first_name" id="first_name" class="form-control" value="<?php echo set_value('first_name'); ?>">

                                <?php if (form_error('first_name')) {
                                    echo form_error('first_name', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>

                            </div>

                            <?php if (form_error('last_name')) { ?>
                                <div class="form-group has-error">
                            <?php } else { ?>
                                <div class="form-group">
                             <?php } ?>

                                 <label for="last_name">Your lastname</label>
                                 <input type="text" name="last_name" id="last_name" class="form-control" value="<?php echo set_value('last_name'); ?>">

                                 <?php if (form_error('last_name')) {
                                     echo form_error('last_name', '<span id="helpBlock4" class="help-block">', '</span>');
                                 } ?>

                             </div>

                            <?php if (form_error('email')) { ?>
                                <div class="form-group has-error">
                            <?php } else { ?>
                                <div class="form-group">
                            <?php } ?>

                                <label for="email">Your e-mail</label>
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo set_value('email'); ?>">

                                <?php if (form_error('email')) {
                                    echo form_error('email', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>

                            </div>

                            <?php if (form_error('password')) { ?>
                                <div class="form-group has-error">
                            <?php } else { ?>
                                <div class="form-group">
                            <?php } ?>

                                <label for="password">Your password</label>
                                <input type="password" name="password" id="password" class="form-control">

                                <?php if (form_error('password')) {
                                    echo form_error('password', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>

                            </div>

                            <?php if (form_error('confirm_password')) { ?>
                                <div class="form-group has-error">
                            <?php } else { ?>
                                <div class="form-group">
                            <?php } ?>

                                <label for="confirm_password">Your confirm password</label>
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control">

                                <?php if (form_error('confirm_password')) {
                                    echo form_error('confirm_password', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>

                            </div>

                            <button type="submit" class="btn btn-block btn-primary">
                                Register
                            </button>
                        <?php echo form_close(); ?>
                    </div> <!-- / .panel-body -->

                </div> <!-- / .panel -->

                <p class="sign__extra text-muted text-center">
                    Already a member? <a href="<?php echo base_url('auth/login'); ?>">Sign In</a>.
                </p>

            </div> <!-- / .sign__conteiner -->
        </div>
    </div>
</div>

    <!-- JavaScript
    ================================================== -->

    <!-- JS Global -->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>
</html>