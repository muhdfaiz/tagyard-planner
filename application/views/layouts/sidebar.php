<!-- SIDEBAR
 ================================================== -->
<div class="sidebar">

    <!-- Sidebar: Close button (mobile devices) -->
    <div class="sidebar__close">
        <img src="<?php echo base_url('assets/img/icons/icon_close.svg'); ?>" alt="Close sidebar">
    </div>

    <!-- Sidebar: User -->
    <div class="sidebar__user">

        <!-- Sidebar: User avatar -->
        <div class="sidebar-user__avatar">
            <?php if (!empty($this->avatar)) { ?>
                <img class="profile-image" src="<?php echo base_url('uploads/' . $this->avatar); ?>"/>
            <?php } else { ?>
                <img class="profile-image" src="<?php echo base_url('assets/img/default-avatar.png'); ?>'"/>
            <?php } ?>
        </div>

        <!-- Sidebar: User info -->
        <a class="sidebar-user__info" role="button" href="#sidebar-user__nav" data-toggle="collapse" aria-expanded="false" aria-controls="sidebar-user__nav">
            <h4><?php echo $this->fullname; ?></h4>
            <p>Member <i class="fa fa-caret-down"></i></p>
        </a>

    </div> <!-- / .sidebar__user -->

    <!-- Sidebar: User nav -->
    <nav class="sidebar-user__nav collapse" id="sidebar-user__nav">
        <ul class="sidebar__nav">
            <li>
                <a href="<?php echo base_url('/user/edit'); ?>">
                    <i class="fa fa-edit"></i> Edit profile
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('/auth/logout'); ?>">
                    <i class="fa fa-sign-out"></i> Sign out
                </a>
            </li>
        </ul>
    </nav>

    <!-- Sidebar: Nav -->
    <nav id="sidebar__nav">
        <ul class="sidebar__nav">
            <li class="sidebar-nav__heading">Menu</li>

            <li><a href="<?php echo base_url('/dashboard'); ?>">
                    <i class="fa fa-home" aria-hidden="true"></i> Dashboard
                </a>
            </li>

            <li><a href="<?php echo base_url('/tag'); ?>">
                    <i class="fa fa-tag" aria-hidden="true"></i> Tags
                </a>
            </li>

            <li>
                <a href="<?php echo base_url('/planner'); ?>">
                    <i class="fa fa-book" aria-hidden="true"></i> Planners
                </a>
            </li>
        </ul>
    </nav>

</div>