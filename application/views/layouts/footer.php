</div> <!-- / .wrapper -->


<!-- JavaScript
================================================== -->

<!-- JS Global -->
<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

<!-- JS Plugins -->
<script src="<?php echo base_url('assets/plugins/count-to/jquery.countTo.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-switch/bootstrap-switch.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/dragula/dragula.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery-fileuploader/jquery.fileuploader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-sweet-alert/sweetalert.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-3-typeahead/bootstrap3-typeahead.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-star-rating/js/star-rating.min.js'); ?>"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0fw7XFKcPLiMOWSYz9yP56ai6dSKPZ6E&libraries=places"></script>

<!-- JS Custom -->
<script src="<?php echo base_url('assets/js/app.js'); ?>"></script>

</body>
</html>