<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="<?php echo $this->security->get_csrf_token_name(); ?>" content="<?php echo $this->security->get_csrf_hash(); ?>">
    <title><?php echo $title; ?></title>

    <!-- CSS Plugins -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/bootstrap-switch/bootstrap-switch.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/dragula/dragula.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/jquery-fileuploader/jquery.fileuploader.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/bootstrap-sweet-alert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/bootstrap-star-rating/css/star-rating.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/bootstrap.vertical-tabs.min.css'); ?>">


    <!-- CSS Global -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/theme.css'); ?>">
    <script>var base_url = '<?php echo base_url() ?>';</script>
</head>

<body>

<!-- NAVBAR
================================================== -->
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbar_main">

            <!-- Navbar: Brand -->
            <a class="navbar-brand" href="<?php echo base_url('/dashboard')?>">
                Tagyard Planner
            </a>

            <!-- Navbar: Toggle menu -->
            <a href="#" class="navbar-btn navbar-left" id="sidebar__toggle">
                <i class="fa fa-bars"></i>
            </a>

            <!-- Navbar: Sign out -->
            <a href="<?php echo base_url('auth/logout'); ?>" class="navbar-btn navbar-right btn btn-accent">
                Sign Out
            </a>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<!-- WRAPPER
================================================== -->
<div class="wrapper">