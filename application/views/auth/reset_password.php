
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    <!-- CSS Plugins -->
    <link rel="stylesheet" href="/assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css">

    <!-- CSS Global -->
    <link rel="stylesheet" href="/assets/css/theme.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>

</head>

<body>
<!-- MAIN CONTENT
================================================== -->
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

            <!-- Sign In -->
            <div class="sign__container">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Reset your password
                        </h4>
                    </div> <!-- / .panel-heading -->

                    <div class="panel-body">
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                            </div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('message')) { ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Successful!</strong> <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php } ?>

                        <?php echo form_open(base_url('auth/reset_password/' . $code)); ?>
                            <?php echo form_hidden('user_id', $user_id); ?>

                            <?php if (form_error('new_password')) { ?>
                                 <div class="form-group has-error">
                            <?php } else { ?>
                                <div class="form-group">
                            <?php } ?>

                                <label for="new_password">Your new password</label>
                                <input type="password" name="new_password" id="new_password" class="form-control">

                                <?php if (form_error('new_password')) {
                                    echo form_error('new_password', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>

                            </div>

                            <?php if (form_error('confirm_new_password')) { ?>
                                <div class="form-group has-error">
                            <?php } else { ?>
                                <div class="form-group">
                            <?php } ?>

                                <label for="confirm_new_password">Confirm your new password</label>
                                <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control">

                                <?php if (form_error('confirm_new_password')) {
                                    echo form_error('confirm_new_password', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>

                            </div>

                            <button type="submit" class="btn btn-block btn-primary">
                                Change Password
                            </button>

                        <?php echo form_close(); ?>
                    </div> <!-- / .panel-body -->
                </div> <!-- / .panel -->
            </div> <!-- / .sign__conteiner -->
        </div>
    </div>
</div>

<!-- JavaScript
================================================== -->

<!-- JS Global -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="/assets/js/app.js"></script>
</body>
</html>