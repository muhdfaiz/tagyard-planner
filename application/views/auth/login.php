
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    <!-- CSS Plugins -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/jquery-toast-plugin/jquery.toast.min.css'); ?>">

    <!-- CSS Global -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/theme.css'); ?>">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
    <script>var base_url = '<?php echo base_url() ?>';</script>
</head>

<body>
<!-- MAIN CONTENT
================================================== -->
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

            <!-- Sign In -->
            <div class="sign__container">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Sign In to your account
                        </h4>
                    </div> <!-- / .panel-heading -->

                    <div class="panel-body">
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                            </div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('message')) { ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Successful!</strong> <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php } ?>

                        <?php echo form_open(base_url('auth/login')); ?>
                            <?php if (form_error('email')) { ?>
                                <div class="form-group has-error">
                            <?php } else { ?>
                                <div class="form-group">
                            <?php } ?>
                                <label for="email">Your e-mail</label>
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo set_value('email'); ?>">
                                <?php if (form_error('email')) {
                                    echo form_error('email', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>

                            <?php if (form_error('password')) { ?>
                                <div class="form-group has-error">
                            <?php } else { ?>
                                    <div class="form-group">
                            <?php } ?>
                                <label for="password">Your password</label>
                                <input type="password" name="password" id="password" class="form-control">
                                <?php if (form_error('password')) {
                                    echo form_error('password', '<span id="helpBlock4" class="help-block">', '</span>');
                                } ?>
                            </div>

                            <div class="checkbox">
                                <input type="checkbox" name="remember" id="remember">
                                <label for="remember">
                                    Remember me
                                </label>
                            </div>

                            <button type="submit" class="btn btn-block btn-primary">
                                Sign In
                            </button>
                        <?php echo form_close(); ?>
                    </div> <!-- / .panel-body -->

                    <div class="panel-footer">
                        <a href="#" data-toggle="collapse" data-target="#sign__resend-password">Forgot your password?</a>
                        <div class="collapse" id="sign__resend-password">
                            <form id="forgot-password-form" class="form-inline" >
                                <input type="hidden" id="csrf-token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                <div class="form-group">
                                    <label class="email sr-only">Enter your e-mail</label>
                                    <input type="email" id="forgot-password-email" class="form-control" placeholder="Enter your e-mail" required>
                                </div>
                                <button type="button" id="forgot-password" class="btn btn-link">
                                    Send
                                </button>
                            </form>
                        </div>
                    </div> <!-- / .panel-footer -->

                </div> <!-- / .panel -->

                <p class="sign__extra text-muted text-center">
                    Not registered? <a href="<?php echo base_url('register'); ?>">Create an account</a>.
                </p>

            </div> <!-- / .sign__conteiner -->

        </div>
    </div>
</div>

<!-- JavaScript
================================================== -->

<!-- JS Global -->
<script src="<?php echo base_url('/assets/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/plugins/jquery-toast-plugin/jquery.toast.min.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#forgot-password").click(function(event) {
            $.toast({
                heading: 'Sending Password Reset Email...',
                showHideTransition: 'slide',
                icon: 'info',
                hideAfter: false,
                position: 'bottom-right'
            });
            event.preventDefault();
            $.ajax({
                url: base_url + "auth/forgot_password",
                async: false,
                type: "POST",
                data: {
                    'email' : $("#forgot-password-email").val(),
                    'token' : $("#csrf-token").val()
                },
                dataType: "json",
                beforeSend: function(){

                },
                success: function(data) {
                    if (data.hasOwnProperty('error')) {
                        $.toast({
                            heading: 'Error Sending Password Reset Email.',
                            text: data.error,
                            showHideTransition: 'slide',
                            icon: 'error',
                            hideAfter: false,
                            position: 'bottom-right'
                        });
                    }

                    if (data.hasOwnProperty('message')) {
                        $.toast({
                            heading: 'Successful Sent Password Reset Email.',
                            text: 'Please check your email and click the link to reset your password.',
                            showHideTransition: 'slide',
                            icon: 'success',
                            hideAfter: false,
                            position: 'bottom-right'
                        });
                    }
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            })
        });
    });
</script>
</body>
</html>